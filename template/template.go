package template

import (
	"html/template"

	"github.com/sirupsen/logrus"
)

//go:generate fileb0x ab0x.yaml

func Load() *template.Template {
	tpls := template.New("").Funcs(NewFuncMap())
	files := []string{"404.html", "coinintro.html", "user/cash.html", "user/cshop.html", "user/index.html", "user/login.html", "user/register.html", "user/recharge.html", "user/user.html", "user/shop.html", "user/registerSuccess.html", "user/verifySuccess.html", "user/task.html"}

	for _, filename := range files {
		file, err := ReadFile(filename)

		if err != nil {
			logrus.Warnf("Failed to read builtin %s template. %s", filename, err)
		} else {
			tpls.New(
				filename,
			).Parse(
				string(file),
			)
		}
	}

	return tpls
}
