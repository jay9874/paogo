package template

import (
	"encoding/json"
	"html/template"
	"net/url"
	"reflect"
	"strings"
	"time"
)

// recovery will silently swallow all unexpected panics.
func recovery() {
	recover()
}

// NewFuncMap returns functions for injecting to templates
func NewFuncMap() template.FuncMap {
	return template.FuncMap{
		"formatTime": func(raw int64) string {
			defer recovery()

			t := time.Unix(raw, 0)

			return t.Format("Jan 2 15:04:05 2006")
		},
		"Safe": Safe,
		"default": func(arg interface{}, value interface{}) interface{} {
			defer recovery()

			v := reflect.ValueOf(value)
			switch v.Kind() {
			case reflect.String, reflect.Slice, reflect.Array, reflect.Map:
				if v.Len() == 0 {
					return arg
				}
			case reflect.Bool:
				if !v.Bool() {
					return arg
				}
			default:
				return value
			}

			return value
		},
		"length": func(value interface{}) int {
			defer recovery()

			v := reflect.ValueOf(value)
			switch v.Kind() {
			case reflect.Slice, reflect.Array, reflect.Map:
				return v.Len()
			case reflect.String:
				return len([]rune(v.String()))
			}

			return 0
		},
		"lower": func(s string) string {
			defer recovery()

			return strings.ToLower(s)
		},
		"upper": func(s string) string {
			defer recovery()

			return strings.ToUpper(s)
		},
		"urlencode": func(s string) string {
			defer recovery()

			return url.QueryEscape(s)
		},
		"wordcount": func(s string) int {
			defer recovery()

			return len(strings.Fields(s))
		},
		"add": func(s, d int) int {
			defer recovery()

			return s + d
		},
		"marshal": func(v interface{}) template.JS {
			a, _ := json.Marshal(v)
			return template.JS(a)
		},
	}
}

// Safe render raw as HTML
func Safe(raw string) template.HTML {
	defer recovery()

	return template.HTML(raw)
}
