package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Mission struct {
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	StudentID string        `bson:"studentID" json:"studentID"`
	Title     string        `bson:"title" json:"title"`
	Commodity string        `bson:"commodity" json:"commodity"`
	Shopname  string        `bson:"shopname" json:"shopname"`
	Runfee    int           `bson:"runfee" json:"runfee"`
	DeadLine  time.Time     `bson:"deadline" json:"deadline"`
	Remarks   string        `bson:"remarks" json:"remarks"`
	Place     string        `bson:"place" json:"place"`
	Status    int           `bson:"status" json:"status"`
	PublicID  string        `bson:"publicID" json:"publicID"`
	ShopImage []string      `bson:"shopimage" json:"shopimage"`
	CreatedAt time.Time     `bson:"createdAt" json:"createdAt,omitempty"`
}
