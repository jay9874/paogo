package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type UserCash struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id"`
	StudentID   string        `bson:"studentId" json:"studentId"`
	Email       string        `bson:"email" json:"email"`
	OrderNumber string        `bson:"orderNumber" json:"orderNumber"`
	Amt         int           `bson:"Amt" json:"Amt"`
	Status      int           `bson:"status" json:"status"`
	CreatedAt   time.Time     `bson:"createdAt" json:"createdAt"`
	ExpireDate  time.Time     `bson:"expireDate" json:"expireDate"`
}
