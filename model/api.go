package model

import (
	"errors"
	"graduate-project/config"
	"strconv"

	"github.com/sirupsen/logrus"

	"github.com/jinzhu/now"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	DB *API
)

type API struct {
	db *MongoConnection
}

func NewAPI() {
	DB = &API{
		db: NewDBConnection(),
	}
}

func (f *API) getSessionAndCollection(table string) (session *mgo.Session, collection *mgo.Collection, err error) {
	if f.db.originalSession != nil {
		session = f.db.originalSession.Copy()
		collection = session.DB(config.Database.Name).C(table)
	} else {
		err = errors.New("No original session found")
	}
	return
}

func (f *API) CreateUser(row *User) error {
	session, collection, err := f.getSessionAndCollection(config.TableUser)
	defer session.Close()

	if err != nil {
		return err
	}
	err = collection.Insert(row)
	return err
}

func (f *API) QueryUserByID(ID bson.ObjectId) (User, error) {
	session, collection, err := f.getSessionAndCollection(config.TableUser)
	defer session.Close()

	result := User{}
	if err != nil {
		return result, err
	}
	err = collection.FindId(ID).One(&result)

	if err != nil {
		return result, err
	}
	return result, nil
}

func (f *API) QueryUserByStudentID(studentID string) (User, error) {

	session, collection, err := f.getSessionAndCollection(config.TableUser)
	defer session.Close()
	result := User{}

	if err != nil {
		return result, err
	}

	err = collection.Find(bson.M{"studentId": studentID}).One(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) UpdateUser(ID bson.ObjectId, data map[string]interface{}) error {
	session, collection, err := f.getSessionAndCollection(config.TableUser)
	defer session.Close()

	result := User{}
	if err != nil {
		return err
	}

	err = collection.FindId(ID).One(&result)
	if err != nil {
		return err
	}

	err = collection.UpdateId(ID, bson.M{"$set": data})
	if err != nil {
		return err
	}

	return nil
}

func (f *API) CreateMission(row *Mission) error {
	session, collection, err := f.getSessionAndCollection(config.TableMission)

	defer session.Close()
	if err != nil {
		return err
	}

	err = collection.Insert(row)

	return err
}

func (f *API) CountAllMission() (int, error) {
	session, collection, err := f.getSessionAndCollection(config.TableMission)

	defer session.Close()
	if err != nil {
		return 0, err
	}

	count, err := collection.Find(bson.M{
		"$and": []bson.M{
			{"createdAt": bson.M{"$gte": now.BeginningOfDay()}},
			{"createdAt": bson.M{"$lt": now.EndOfDay()}},
		},
	}).Count()

	return count, err
}

func (f *API) GetAllMission() ([]Mission, error) {
	session, collection, err := f.getSessionAndCollection(config.TableMission)

	defer session.Close()
	result := []Mission{}
	if err != nil {
		return result, err
	}

	err = collection.Find(bson.M{
		"$and": []bson.M{
			{"createdAt": bson.M{"$gte": now.BeginningOfDay()}},
			{"createdAt": bson.M{"$lt": now.EndOfDay()}},
			//{"deadline": bson.M{"$gte": time.Now()}},
		},
	}).All(&result)

	return result, err
}

func (f *API) QueryMission(studentID string) ([]Mission, error) {
	session, collection, err := f.getSessionAndCollection(config.TableMission)
	defer session.Close()
	result := []Mission{}

	if err != nil {
		return result, err
	}
	err = collection.Find(bson.M{"studentID": studentID}).All(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) QueryOrderByPublicID(publicID string) ([]Order, error) {
	session, collection, err := f.getSessionAndCollection(config.TableOrder)
	defer session.Close()
	result := []Order{}

	if err != nil {
		return result, err
	}
	err = collection.Find(bson.M{"missionID": publicID}).All(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) QueryMissionByPublicID(publicID string) (Mission, error) {
	session, collection, err := f.getSessionAndCollection(config.TableMission)
	defer session.Close()
	result := Mission{}

	if err != nil {
		return result, err
	}
	err = collection.Find(bson.M{"publicID": publicID}).One(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) UpdateStore(id bson.ObjectId, data map[string]interface{}) (interface{}, error) {
	session, collection, err := f.getSessionAndCollection(config.TableMission)
	defer session.Close()
	result := Mission{}
	if err != nil {
		return result, err
	}

	err = collection.FindId(id).One(&result)
	if err != nil {
		return result, err
	}

	err = collection.UpdateId(id, bson.M{"$set": data})
	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) CreateCashOrder(row *UserCash) error {
	session, collection, err := f.getSessionAndCollection(config.TableCash)
	defer session.Close()

	if err != nil {
		return err
	}
	err = collection.Insert(row)
	return err
}

func (f *API) QueryCashByOrderNo(orderNum string) (UserCash, error) {
	session, collection, err := f.getSessionAndCollection(config.TableCash)
	defer session.Close()

	result := UserCash{}

	if err != nil {
		return result, err
	}

	err = collection.Find(bson.M{"orderNumber": orderNum}).One(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) UpdateCashByOrderNo(orderNum string, data map[string]interface{}) (UserCash, error) {
	session, collection, err := f.getSessionAndCollection(config.TableCash)
	defer session.Close()

	result := UserCash{}

	if err != nil {
		return result, err
	}

	err = collection.Find(bson.M{"orderNumber": orderNum}).One(&result)
	if err != nil {
		return result, err
	}

	err = collection.Update(bson.M{"orderNumber": orderNum}, bson.M{"$set": data})
	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) CreateSMS(row *SMS) error {
	session, collection, err := f.getSessionAndCollection(config.TableSMS)
	defer session.Close()

	if err != nil {
		return err
	}
	err = collection.Insert(row)
	return err
}

func (f *API) CreateOrder(row *Order) error {
	session, collection, err := f.getSessionAndCollection(config.TableOrder)
	defer session.Close()

	if err != nil {
		return err
	}
	err = collection.Insert(row)
	return err
}

func (f *API) CreateChat(row *Chat) error {
	session, collection, err := f.getSessionAndCollection(config.TableChat)

	defer session.Close()
	if err != nil {
		return err
	}

	err = collection.Insert(row)

	return err
}

func (f *API) FindChatMember(members ...string) error {
	session, collection, err := f.getSessionAndCollection(config.TableChat)
	defer session.Close()
	result := Chat{}
	if err != nil {
		return err
	}

	err = collection.Find(bson.M{"members": bson.M{"$all": members}}).One(&result)

	if err != nil {
		return err
	}

	return nil
}

func (f *API) GetChatMsg(members []string) (Chat, error) {
	session, collection, err := f.getSessionAndCollection(config.TableChat)
	defer session.Close()
	result := Chat{}

	logrus.Println(members)
	query := []bson.M{
		{"$match": bson.M{
			"members": &bson.M{"$all": members}}},
		//{"$unwind": "messages"},
		{"$sort": bson.M{"messages.msgCreatedAt": 1}},
	}

	err = collection.Pipe(query).One(&result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) UpdateChatMsg(members []string, msg Message) error {
	session, collection, err := f.getSessionAndCollection(config.TableChat)
	defer session.Close()

	if err != nil {
		return err
	}

	err = collection.Update(bson.M{"members": bson.M{"$all": members}}, bson.M{"$push": bson.M{"messages": msg}})
	if err != nil {
		return err
	}

	return nil
}

func (f *API) UpdateIsRead(msgID string, members []string) error {
	session, collection, err := f.getSessionAndCollection(config.TableChat)
	defer session.Close()
	if err != nil {
		return err
	}

	chatHistory, _ := f.GetChatMsg(members)

	for k, v := range chatHistory.Messages {
		if v.ID == msgID {
			err = collection.Update(bson.M{"members": bson.M{"$in": members}, "messages.msgID": msgID}, bson.M{"$set": bson.M{"messages." + strconv.Itoa(k) + ".isRead": true}})
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (f *API) QueryOrder(studentID string) ([]Order, error) {
	session, collection, err := f.getSessionAndCollection(config.TableOrder)
	defer session.Close()
	result := []Order{}

	if err != nil {
		return result, err
	}
	err = collection.Find(bson.M{"studentID": studentID}).All(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

func (f *API) UpdateMissionByPublicID(publicID string, data map[string]interface{}) error {
	session, collection, err := f.getSessionAndCollection(config.TableMission)
	defer session.Close()

	result := Mission{}

	if err != nil {
		return err
	}

	err = collection.Find(bson.M{"publicID": publicID}).One(&result)
	if err != nil {
		return err
	}

	err = collection.Update(bson.M{"publicID": publicID}, bson.M{"$set": data})
	if err != nil {
		return err
	}

	return nil
}
