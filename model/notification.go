package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type NotificationType int

// Note: new type must append to the end of list to maintain compatibility.
const (
	NewOrderType     NotificationType = iota + 1
	PayCompletedType                  // 2
	ContactUsType                     // 3
)

func (c NotificationType) String() string {
	if int(c) > len(notificationCodeMap) {
		return ""
	}

	return notificationCodeMap[c]
}

var notificationCodeMap = map[NotificationType]string{
	NewOrderType:     "NewOrder",
	PayCompletedType: "PayCompleted",
	ContactUsType:    "ContactUs",
}

// Notification for notification list of user panel
type Notification struct {
	ID        bson.ObjectId    `bson:"_id,omitempty" json:"id,omitempty"`
	StudentID string           `bson:"studentId" json:"studentId,omitempty"`
	TypeID    NotificationType `bson:"typeId" json:"typeId,omitempty"`
	TypeName  string           `bson:"-" json:"typeName,omitempty"`
	Format    string           `bson:"format" json:"format,omitempty"`
	Data      string           `bson:"data" json:"data,omitempty"`
	IsRead    bool             `bson:"isRead" json:"isRead,omitempty"`
	CreatedAt time.Time        `bson:"createdAt" json:"createdAt,omitempty"`
}
