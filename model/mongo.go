package model

import (
	"fmt"

	"graduate-project/config"

	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2"
)

// MongoConnection for mongodb connection
type MongoConnection struct {
	originalSession *mgo.Session
}

// NewDBConnection for new mongodb connection
func NewDBConnection() (conn *MongoConnection) {
	conn = new(MongoConnection)
	conn.createLocalConnection()
	return
}

func (c *MongoConnection) createLocalConnection() (err error) {
	if config.Database.Driver != "mongodb" {
		logrus.Warnf("Your database driver is %s not mongodb, please set correct database type.", config.Database.Driver)
		return nil
	}

	connectstring := fmt.Sprintf("%s://%s", config.Database.Driver, config.Database.Host)

	logrus.Infof("Connecting to local mongo server....")
	c.originalSession, err = mgo.Dial(connectstring)
	if err != nil {
		logrus.Infof("Error occurred while creating mongodb connection: %s", err.Error())
		return
	}

	// Reads may not be entirely up-to-date, but they will always see the
	// history of changes moving forward, the data read will be consistent
	// across sequential queries in the same session, and modifications made
	// within the session will be observed in following queries (read-your-writes).
	// http://godoc.org/labix.org/v2/mgo#Session.SetMode
	c.originalSession.SetMode(mgo.Monotonic, true)

	return nil
}
