package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type SMS struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id"`
	StudentID   string        `bson:"studentId" json:"studentId"`
	MissionID   bson.ObjectId `bson:"missionID,omitempty" json:"missionID"`
	Status      int           `bson:"status" json:"status"` // failed:0 updateCredits failed:1 success:2
	PhoneNumber string        `bson:"phone" json:"phone"`
	TwilioID    string        `bson:"twilioID" json:"twilioID"`
	CreatedAt   time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time     `bson:"updated_at" json:"updated_at"`
}
