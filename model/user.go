package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type User struct {
	ID           bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Name         string        `bson:"name" json:"name"`
	StudentID    string        `bson:"studentId" json:"studentId"`
	Password     string        `bson:"password" json:"password"`
	Gender       int           `bson:"gender" json:"gender"`
	Email        string        `bson:"email" json:"email"`
	Status       int           `bson:"status" json:"status"`
	Exp          int           `bson:"exp" json:"exp"`
	LineID       string        `bson:"lineId" json:"lineId"`
	ProfileImage string        `bson:"profile_image" json:"profile_image"`
	Token        string        `bson:"token" json:"token"`
	DeviceToken  string        `bson:"device_token" json:"device_token"`
	Development  string        `bson:"development" json:"development"`
	Credits      int           `bson:"credits" json:"credits"`
	Phone        string        `bson:"phone" json:"phone"`
	SendSMS      string        `bson:"sendSMS" json:"sendSMS"`
	CreatedAt    time.Time     `bson:"created_at" json:"created_at"`
	UpdatedAt    time.Time     `bson:"updated_at" json:"updated_at"`
}

func NewUser() *User {
	return &User{
		Status:      0,
		Token:       "",
		DeviceToken: "",
		Exp:         0,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Time{},
	}
}
