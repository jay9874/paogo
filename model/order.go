package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Order struct {
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	StudentID string        `bson:"studentID" json:"studentID"`
	MissionID string        `bson:"missionID" json:"missionID"`
	CreatedAt time.Time     `bson:"createdAt" json:"createdAt,omitempty"`
}
