package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Chat struct {
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Members   []string      `bson:"members" json:"members"`
	Messages  []Message     `bson:"messages" json:"messages"`
	CreatedAt time.Time     `bson:"createdAt" json:"createdAt,omitempty"`
}

type Message struct {
	ID        string    `bson:"msgID,omitempty" json:"msgID"`
	Sender    string    `bson:"sender" json:"sender"`
	Content   string    `bson:"content" json:"content"`
	IsRead    bool      `bson:"isRead" json:"isRead"`
	CreatedAt time.Time `bson:"msgCreatedAt" json:"msgCreatedAt,omitempty"`
}
