package config

const (
	TableUser    = "users"
	TableMission = "missions"
	TableCash    = "user_cashes"
	TableSMS     = "sms"
	TableOrder   = "orders"
	TableChat    = "chats"
)
