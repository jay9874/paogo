package config

type database struct {
	Driver   string
	Username string
	Password string
	Name     string
	Host     string
}

type server struct {
	Host      string
	Addr      string
	Cert      string
	Key       string
	Root      string
	Storage   string
	Assets    string
	ImageAddr string
	Token     string
	Pprof     bool
	OnSite    bool
}

type user struct {
	HashKey string
}

type jwt struct {
	VerifyKey string
	SignKey   string
}

type minio struct {
	AccessID  string
	SecretKey string
	EndPoint  string
	SSL       bool
	Bucket    string
	Region    string
}

type spgateway struct {
	API        string
	MerchantID string
	HashKey    string
	HashIV     string
}

type sms struct {
	Open bool
}

type vault struct {
	Token string
}

type redis struct {
	Host string
	Port string
	Addr string
}

var (
	Debug     bool
	Database  = &database{}
	Server    = &server{}
	User      = &user{}
	JWT       = &jwt{}
	Spgateway = &spgateway{}
	Minio     = &minio{}
	SMS       = &sms{}
	Vault     = &vault{}
	Redis     = &redis{}
)
