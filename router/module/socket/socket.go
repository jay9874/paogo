package socket

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"graduate-project/config"
	"graduate-project/model"
	"graduate-project/web/fcm"
	"io/ioutil"
	"math/rand"
	"net/http"
	"reflect"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/googollee/go-socket.io"
	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

const (
	NotificationRoom = "notification"
)

// Server for socket server
var Server *socketio.Server
var err error
var key = "user"
var QueueNotification chan PushNotification

// Data for socket message
type Data struct {
	HotelID string `json:"hotelID,omitempty"`
}

type welcome struct {
	Code     int    `json:"code"`
	Message  string `json:"message"`
	OnlineID string `json:"onlineID"`
}

type invite struct {
	Inviter   string `json:"inviter"`
	RoomToken string `json:"roomToken"`
}

type PushNotification struct {
	Members   []string `json:"members"`
	Message   string   `json:"message"`
	StudentID string   `json:"studentID"`
	MsgID     string   `json:"msgID"`
	Sender    string   `json:"sender"`
}

// NewEngine for socket server
func NewEngine() error {
	Server, err = socketio.NewServer(nil)
	if err != nil {
		logrus.Debugf("create socker server error: %s", err.Error())
		return err
	}

	var user jwt.MapClaims

	Server.SetAllowRequest(func(r *http.Request) error {
		token := r.URL.Query().Get("token")

		if token == "" {
			return errors.New("Required authorization token not found")
		}

		parts := strings.SplitN(token, " ", 2)

		SignKey, err := ioutil.ReadFile("./userpem/app.rsa")
		if err != nil {
			return errors.New("OpenSSl Can't Found")
		}

		parsedToken, err := jwt.Parse(parts[1], func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return SignKey, nil
		})

		if err != nil {
			return fmt.Errorf("Error parsing token: %v", err)
		}

		/*if jwt.SigningMethodHMAC.Alg() != parsedToken.Header["alg"] {
			message := fmt.Sprintf("Expected %s signing method but token specified %s",
				jwt.SigningMethodHMAC.Alg(),
				parsedToken.Header["alg"])
			return fmt.Errorf("Error validating token algorithm: %s", message)
		}*/

		if !parsedToken.Valid {
			return errors.New("Token is invalid")
		}

		user = parsedToken.Claims.(jwt.MapClaims)

		// If we get here, everything worked and we can set the
		// user property in context.
		newRequest := r.WithContext(context.WithValue(r.Context(), key, parsedToken))
		// Update the current request with the new context information.
		*r = *newRequest

		return nil
	})

	Server.On("connection", func(so socketio.Socket, parsedToken *jwt.Token) {
		logrus.Println("Success")

		room := user["id"].(string)
		logrus.Debugf("room is %s", room)
		so.Join(room)
		so.Join("ServerStuff")

		/*so.BroadcastTo(room, "chat", welcome{
			Code:     308,
			Message:  fmt.Sprintf("Friend %s join the room", user["id"]),
			OnlineID: fmt.Sprintf("%s", user["id"]),
		})*/

		Server.BroadcastTo(room, "userDetail", map[string]interface{}{
			"room": room,
		})

		Server.BroadcastTo(room, "chat", welcome{
			Code:    308,
			Message: fmt.Sprintf("Hi %s, welcome to join the room", user["id"]),
		})

		// receive message
		so.On("chat", func(msg string) {
		})

		so.On("sdp", func(data interface{}) {
			if v, ok := data.(map[string]interface{}); ok {
				so.BroadcastTo(v["room"].(string), "sdp", data)
			}
		})

		so.On("candidate", func(data interface{}) {
			if v, ok := data.(map[string]interface{}); ok {
				so.BroadcastTo(v["room"].(string), "candidate", data)
			}
		})

		so.On("invite", func(data interface{}) {
			if v, ok := data.(map[string]interface{}); ok {
				so.BroadcastTo(v["inviter"].(string), "invite-notification", map[string]interface{}{
					"inviter":   user["id"].(string),
					"roomToken": v["roomToken"].(string),
				})
			} else {
				logrus.Println(data)
				logrus.Println("not ok")
			}

		})

		so.On("cancelInvite", func(data interface{}) {

			if v, ok := data.(map[string]interface{}); ok {
				so.BroadcastTo(v["inviter"].(string), "cancel-notification", map[string]interface{}{
					"inviter": v["inviter"].(string),
				})
			}

		})

		so.On("join", func(newroom string) {
			so.Join(newroom)
			Server.BroadcastTo(newroom, "newRoom", map[string]interface{}{
				"msg": "You are in new room " + newroom,
			})
		})

		so.On("chat-with", func(data interface{}) {
			logrus.Println(data)
			if v, ok := data.(map[string]interface{}); ok {
				token := v["token"].(string)
				receiver := v["receiver"].(string)
				message := v["msg"].(string)
				sender, err := ParseUserByToken(token)

				if err != nil {
					sender = ""
				}
				loc, err := time.LoadLocation("UTC")
				if err != nil {
					logrus.Println("Chat parse time error")
					logrus.Println(err)
				}
				now := time.Now().In(loc)
				msgID := CreateChatRelation(sender, receiver, message, now)
				Server.BroadcastTo(receiver, "ReceiveMsg", map[string]interface{}{
					"sender":       sender,
					"msg":          message,
					"isRead":       false,
					"msgID":        msgID,
					"msgCreatedAt": now,
				})

				newNotification := PushNotification{
					Message:   message,
					StudentID: receiver,
					MsgID:     msgID,
					Members:   []string{sender, receiver},
					Sender:    sender,
				}

				QueueNotification <- newNotification

			} else {
				logrus.Println("It not ok")
			}
		})

		so.On("getRoomInfo", func(data interface{}) {
			logrus.Println("Run to Info")
			if v, ok := data.(map[string]interface{}); ok {
				logrus.Println("Get Info Success")
				receiver := v["receiver"].(string)
				member := v["member"].(string)

				user, _ := model.DB.QueryUserByStudentID(member)
				Server.BroadcastTo(receiver, "RoomInfo", map[string]interface{}{
					"roomToken": randSeq(10),
					"MemberPic": user.ProfileImage,
				})

			} else {
				logrus.Println("Get info error")
			}
		})

		so.On("isRead", func(data interface{}) {
			logrus.Println(data)
			if v, ok := data.(map[string]interface{}); ok {
				token := ""
				receiver := ""
				msgID := ""

				if k, ok := v["token"].(string); ok {
					token = k
				} else {
					return
				}

				if k, ok := v["receiver"].(string); ok {
					receiver = k
				} else {
					return
				}

				if k, ok := v["msgID"].(string); ok {
					msgID = k
				} else {
					return
				}

				sender, err := ParseUserByToken(token)

				if err != nil {
					sender = ""
				}
				go UpdateIsRead(msgID, sender, receiver)
				Server.BroadcastTo(receiver, "isReadClient", map[string]interface{}{
					"isRead": true,
					"msgID":  msgID,
				})

			}
		})

		so.On("startwebrtc", func(room string) {
			Server.BroadcastTo(room, "startwebrtc")
		})

		so.On("disconnection", func() {
			logrus.Debugln("client disconnection")
		})
	})

	Server.On("error", func(so socketio.Socket, err error) {
		logrus.Debugf("socker server error: %s", err.Error())
	})

	return nil
}

func CreateChatRelation(sender, receiver, message string, now time.Time) string {
	//sender, err := ParseUserByToken(token)
	members := []string{sender, receiver}
	if err != nil {
		return ""
	}

	if err = model.DB.FindChatMember(sender, receiver); err != nil {
		logrus.Println("Chats is not found")
		row := model.Chat{
			ID:        bson.NewObjectId(),
			Members:   members,
			Messages:  []model.Message{},
			CreatedAt: now,
		}
		err = model.DB.CreateChat(&row)

		if err != nil {
			logrus.Println(err)
			return ""
		}
	}
	row := model.Message{
		ID:        bson.NewObjectId().Hex(),
		Sender:    sender,
		Content:   message,
		IsRead:    false,
		CreatedAt: now,
	}
	_ = model.DB.UpdateChatMsg(members, row)

	return row.ID

}

func UpdateIsRead(msgID string, members ...string) {
	logrus.Println("Go to Update", msgID)

	err := model.DB.UpdateIsRead(msgID, members)

	if err != nil {
		logrus.Println(err)
	}

}

func ParseUserByToken(token string) (string, error) {
	parts := strings.SplitN(token, " ", 2)

	SignKey, err := ioutil.ReadFile("./userpem/app.rsa")
	if err != nil {
		return "", errors.New("OpenSSl Can't Found")
	}

	parsedToken, err := jwt.Parse(parts[1], func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return SignKey, nil
	})

	if err != nil {
		return "", fmt.Errorf("Error parsing token: %v", err)
	}

	/*if jwt.SigningMethodHMAC.Alg() != parsedToken.Header["alg"] {
		message := fmt.Sprintf("Expected %s signing method but token specified %s",
			jwt.SigningMethodHMAC.Alg(),
			parsedToken.Header["alg"])
		return fmt.Errorf("Error validating token algorithm: %s", message)
	}*/

	if !parsedToken.Valid {
		return "", errors.New("Token is invalid")
	}

	user := parsedToken.Claims.(jwt.MapClaims)

	return user["id"].(string), nil
}

// BroadcastTo is a server level broadcast function.
func BroadcastTo(room string, studentID string, notification model.NotificationType, data interface{}) {

	m := model.Notification{
		ID:        bson.NewObjectId(),
		TypeID:    notification,
		StudentID: studentID,
		TypeName:  notification.String(),
		IsRead:    false,
		CreatedAt: time.Now(),
	}

	if data != nil {
		if reflect.TypeOf(data).Kind() == reflect.Struct {
			data, _ := json.Marshal(data)
			m.Data = string(data)
		}
	}

	// Save notification
	//go model.DB.CreateNotification(&m)

	Server.BroadcastTo(
		room,
		NotificationRoom,
		m,
	)
}

// NewMission is for notify client.
func NewMission(publicID string) {
	Server.BroadcastTo(
		"ServerStuff",
		"NewMission",
		publicID,
	)

}

func AcceptOrder(publicID string) {
	Server.BroadcastTo(
		"ServerStuff",
		"AcceptOrder",
		publicID,
	)
}

func randSeq(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func init() {

	QueueNotification = make(chan PushNotification, 10)

	for i := 0; i <= 5; i++ {
		go startWorker()
	}
}

func startWorker() {
	for {
		notification := <-QueueNotification
		SendNotification(notification)
	}
}

func SendNotification(newnotification PushNotification) {
	time.Sleep(500 * time.Millisecond)

	msg, _ := model.DB.GetChatMsg(newnotification.Members)

	sender, _ := model.DB.QueryUserByStudentID(newnotification.Sender)

	for _, s := range msg.Messages {
		if s.ID == newnotification.MsgID {
			if s.IsRead == false {
				var client = ConnectRedis()
				defer client.Close()
				if val, err := client.Get("Device-" + newnotification.StudentID).Result(); err != redis.Nil {

					var FCM *fcm.FCMClient
					if err != nil {
						logrus.Println(err)
						return
					}
					FCM, err = fcm.NewFCMClient("AAAAFwyBLeQ:APA91bFWvpSiHzWfiDtBUAN61aHLIutgtdkRpiq0dF1ZK8bDBShnXp8Ln9cZcrtqKeWVEZDRJZropXhvbOohsoo25bJGeVhNnw8WgjOR1uQAEpVJocyjAFhX6GuE_eVx_BA3uB6uUDNy")
					if err != nil {
						logrus.Println(err)
						return
					}

					message := &fcm.Message{
						To: val,
					}
					notification := &fcm.Notification{
						Title: sender.Name,
						Body:  newnotification.Message,
						Sound: "default",
					}
					message.Priority = "high"

					message.Notification = notification

					test, err := FCM.Send(message)
					if err != nil {
						logrus.Println(err)
						logrus.Println(test)
					}
				}
			}
		}
	}

}

func ConnectRedis() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addr,
		Password: "",
		DB:       0,
	})
}

// Handler initializes the prometheus middleware.
func Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		origin := c.GetHeader("Origin")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Origin", origin)
		Server.ServeHTTP(c.Writer, c.Request)
	}
}
