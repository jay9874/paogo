package minio

import (
	"bytes"
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	minio "github.com/minio/minio-go"
)

// Minio client
type Minio struct {
	host   string
	client *minio.Client
}

// NewEngine struct
func NewEngine(endpoint, accessID, secretKey string, ssl bool) (*Minio, error) {

	if endpoint == "" || accessID == "" || secretKey == "" {
		return nil, errors.New("endpoint, accessID and secretKey can't be empty")
	}

	client, err := minio.New(endpoint, accessID, secretKey, ssl)

	if err != nil {
		return nil, err
	}

	host := ""
	if ssl {
		host = "https://" + endpoint
	} else {
		host = "http://" + endpoint
	}

	return &Minio{
		host:   host,
		client: client,
	}, nil
}

// CreateBucket create bucket
func (m *Minio) CreateBucket(bucketName, region string) error {
	exists, err := m.client.BucketExists(bucketName)
	if err != nil {
		return err
	}

	if exists {
		logrus.Infof("We already own %s bucket", bucketName)
		return nil
	}
	logrus.Println(bucketName)
	logrus.Println(region)
	if err := m.client.MakeBucket(bucketName, region); err != nil {
		return err
	}

	logrus.Infof("Successfully created s3 bucket: %s", bucketName)

	return nil
}

func (m *Minio) UploadFile(bucketName, objectName, filePath string, content []byte, contentType string) error {

	_, err := m.client.PutObject(
		bucketName,
		objectName,
		bytes.NewReader(content),
		int64(len(content)),
		minio.PutObjectOptions{ContentType: contentType},
	)
	return err
}

// FilePath for store path + file name
func (m *Minio) FilePath(fileName string) string {
	return fmt.Sprintf("%s/%s", os.TempDir(), fileName)
}

// GetFile for storage host + bucket + filename
func (m *Minio) GetFile(bucketName, fileName string) string {
	return m.host + "/" + bucketName + "/" + fileName
}
