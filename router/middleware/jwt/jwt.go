package jwt

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	privKeyPath = "./userpem/app.rsa"
	pubKeyPath  = "./userpem/app.rsa.pub"
)

func JwtAuth(c *gin.Context) {
	authHeader := c.Request.Header.Get("Authorization")

	if authHeader == "" {
		c.AbortWithStatusJSON(
			http.StatusBadRequest,
			gin.H{
				"code":    402,
				"Message": "Auth Header Empty.",
			},
		)
		return
	}

	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == "Bearer") {
		c.AbortWithStatusJSON(
			http.StatusBadRequest,
			gin.H{
				"code":    402,
				"Message": "Invalid Auth Header.",
			},
		)
		return
	}
	SignKey, err := ioutil.ReadFile(privKeyPath)
	if err != nil {
		c.AbortWithStatusJSON(
			http.StatusInternalServerError,
			gin.H{
				"code":    301,
				"message": "OpenSSl Can't Found",
			},
		)
		return
	}
	token, err := jwt.Parse(parts[1], func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return SignKey, nil
	})
	if err != nil {
		c.AbortWithStatusJSON(
			http.StatusBadRequest,
			gin.H{
				"code":    403,
				"message": "Unexpected Signing Method",
			},
		)
	}
	claims := token.Claims.(jwt.MapClaims)

	origIat := int64(claims["orig_iat"].(float64))

	if origIat < time.Now().Add(-time.Hour*24*7).Unix() {
		c.AbortWithStatusJSON(
			http.StatusUnauthorized,
			gin.H{
				"message": "Token is expired.",
			},
		)
		return
	}

	newToken := jwt.New(jwt.GetSigningMethod("HS256"))
	newClaims := newToken.Claims.(jwt.MapClaims)

	for key := range claims {
		newClaims[key] = claims[key]
	}

	newClaims["id"] = claims["id"]
	newClaims["exp"] = time.Now().Add(time.Hour).Unix()
	newClaims["orig_iat"] = origIat

	tokenString, err := newToken.SignedString(SignKey)

	if err != nil {
		c.AbortWithStatusJSON(
			http.StatusUnauthorized,
			gin.H{
				"message": "Create JWT Token Faild",
			},
		)
		return
	}
	c.Writer.Header().Set("Authorization", "Bearer "+tokenString)
	c.Set("userStudentID", newClaims["id"])
	logrus.Debugf("new token: %s", tokenString)
	c.Next()
}
