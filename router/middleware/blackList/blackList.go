package blackList

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

var client = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "",
	DB:       0,
})

func CheckIP(c *gin.Context) {
	logrus.Println(c.ClientIP())
	ip := c.ClientIP()
	loginTime, err := client.Get(ip).Result()

	if err == redis.Nil {
		err = client.Set(ip, 1, time.Hour).Err()

		if err != nil {
			logrus.Debug("error", err)
		}
	}
	if v, err := strconv.Atoi(loginTime); err == nil {
		if v > 100 {
			c.AbortWithStatusJSON(
				http.StatusBadRequest,
				gin.H{
					"code":    700,
					"Message": "Out of max retry in an hour.",
				},
			)
			return

		} else {
			err := client.Set(ip, v+1, time.Hour).Err()

			if err != nil {
				logrus.Debug("error", err)
			}
		}
	}
	logrus.Println(loginTime)
	c.Next()

}
