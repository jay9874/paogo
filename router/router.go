package router

import (
	"fmt"
	"graduate-project/assets"
	"graduate-project/config"
	"graduate-project/helper"
	"graduate-project/model"
	"graduate-project/router/middleware/jwt"
	"graduate-project/router/middleware/logger"
	"graduate-project/router/module/socket"
	"graduate-project/router/module/storage"
	"graduate-project/web"
	"graduate-project/web/blockchain"
	"net/http"
	"path"

	"graduate-project/router/middleware/header"

	"graduate-project/template"

	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func GlobalInit() {
	var err error
	// initial socket module
	if err = socket.NewEngine(); err != nil {
		logrus.Fatalf("Failed to initialize Socket IO engine: %v", err)
	}

	storage.S3, err = storage.NewEngine()
	if err != nil {
		logrus.Fatalf("Failed to create s3 interface: %v", err)
	}

	if err := storage.S3.CreateBucket(config.Minio.Bucket, config.Minio.Region); err != nil {
		logrus.Fatalf("Failed to create s3 bucket: %v", err)
	}
	if config.Server.OnSite {
		wallets, _ := Blockchain.NewWallets("3001")
		address := wallets.CreateWallet()
		wallets.SaveToFile("3001")

		fmt.Printf("Your new address: %s\n", address)
		bc := Blockchain.CreateBlockchain(address, "3001")
		defer bc.DB.Close()
		UTXOSet := Blockchain.UTXOSet{bc}
		UTXOSet.Reindex()

		/*go func(address string) {
			vaultClient := &Vault.Request{}
			vaultClient.Method = "POST"
			vaultClient.URL, err = url.Parse("http://127.0.0.1:8200/v1/secret/WJW")
			if err != nil {
				return
			}
			vaultClient.ClientToken = config.Vault.Token
			vaultClient.SetJSONBody(map[string]interface{}{
				"Wallet": address,
			})
			req, err := vaultClient.ToHTTP()
			if err != nil {
				logrus.Println(err)
				return
			}
			client := &http.Client{}
			_, err = client.Do(req)

			if err != nil {
				logrus.Panicln(err)
				return
			}
		}(address)*/
	}
	helper.CronNewEngine()
}

func Load(middleware ...gin.HandlerFunc) http.Handler {
	helper.StartLog()
	if config.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	e := gin.New()
	e.SetHTMLTemplate(
		template.Load(),
	)
	e.Use(logger.SetLogger())
	e.Use(gin.Recovery())
	e.Use(header.Options)
	e.Use(header.Secure)
	e.Use(middleware...)
	e.Use(gzip.Gzip(gzip.DefaultCompression))
	if config.Server.Pprof {
		pprof.Register(
			e,
			&pprof.Options{
				RoutePrefix: path.Join(config.Server.Root, "debug", "pprof"),
			},
		)
	}
	e.NoRoute(web.NotFound)
	root := e.Group(config.Server.Root)
	{
		model.NewAPI()

		root.StaticFS(
			"/assets",
			assets.Load(),
		)

		root.GET("", func(c *gin.Context) {
			//socket.UpdateIsRead("5adb1e72c6b505000121452a", "F74473148", "F74473067")
			c.HTML(
				http.StatusOK,
				"user/index.html",
				gin.H{},
			)
		})
		root.GET("/coinIntro", func(c *gin.Context) {
			c.HTML(
				http.StatusOK,
				"coinintro.html",
				gin.H{},
			)
		})
		root.GET("/cash", func(c *gin.Context) {
			c.HTML(
				http.StatusOK,
				"user/cash.html",
				gin.H{},
			)
		})
		//root.GET("/test/:token", Blockchain.TestBlock)
		user := e.Group("/user")
		{
			user.GET("/register/success", func(c *gin.Context) {
				c.HTML(
					http.StatusOK,
					"user/registerSuccess.html",
					gin.H{},
				)
			})
			user.GET("/task", func(c *gin.Context) {
				c.HTML(
					http.StatusOK,
					"user/task.html",
					gin.H{},
				)
			})
			user.POST("/task", jwt.JwtAuth, web.GetUserTask)
			user.GET("/detail", web.UserDetailView)
			user.POST("/detail", jwt.JwtAuth, web.UserDetail)
			user.GET("/register", web.UserRegister)
			user.GET("/upload", web.MinioUpload)
			user.POST("/create", web.CreateUser)
			user.GET("/login", web.UserLoginView)
			user.POST("/login", web.UserLogin)
			user.POST("/update", jwt.JwtAuth, web.UpdateUser)
			user.GET("/cash", web.StoreCash)
			user.POST("/cash/order", jwt.JwtAuth, web.CashOrder)
			user.POST("/mission/update", jwt.JwtAuth, web.FinishMission)
			user.GET("/regisert_verification/:token", web.VerifyRegister)
		}

		store := e.Group("/store")
		{
			store.GET("/allStore", func(c *gin.Context) {
				c.HTML(
					http.StatusOK,
					"user/shop.html",
					gin.H{},
				)
			})
			store.GET("/createStore", web.CreateStoreView)
			store.POST("/create", jwt.JwtAuth, web.CreateStore)
			store.GET("/information", web.GetAllStore)
		}

		order := e.Group("/order")
		{
			order.POST("/create", jwt.JwtAuth, web.CreateOrder)
			order.POST("/update", jwt.JwtAuth, web.UpdateShopping)
		}

		sp := e.Group("/spgateway")
		{
			sp.POST("/customer", web.Customer)
		}

		chat := e.Group("/chat")
		{
			chat.POST("/history", jwt.JwtAuth, web.GetChatHistory)
		}

		// socket connection
		root.GET("/socket.io/", socket.Handler())
		root.POST("/socket.io/", socket.Handler())
		root.Handle("WS", "/socket.io", socket.Handler())
		root.Handle("WSS", "/socket.io", socket.Handler())
	}

	return e
}
