package helper

import "github.com/robfig/cron"

// CronNewEngine for cronjob
func CronNewEngine() {
	c := cron.New()
	c.AddFunc("@daily", StartLog)
	c.Start()
}
