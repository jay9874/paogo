package helper

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

var Logger = logrus.New()

func StartLog() {
	Logger.Out = os.Stdout
	if _, err := os.Stat("Log"); os.IsNotExist(err) {
		os.MkdirAll("Log", os.ModePerm)
	}
	current_time := time.Now().Local()
	file, err := os.OpenFile("Log/"+current_time.Format("2006-01-02")+".log", os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		Logger.Out = file
	} else {
		Logger.Info("Failed to log to file, using default stderr")
	}
}
