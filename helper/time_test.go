package helper

import (
	"testing"
	"time"
)

func TestPastDay(t *testing.T) {
	type args struct {
		current time.Time
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "check if past day",
			args: args{
				current: time.Now().AddDate(0, 0, -1),
			},
			want: true,
		},
		{
			name: "check if now",
			args: args{
				current: time.Now(),
			},
			want: false,
		},
		{
			name: "check if tomorrow",
			args: args{
				current: time.Now().AddDate(0, 0, 1),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PastDay(tt.args.current); got != tt.want {
				t.Errorf("PastDay() = %v, want %v", got, tt.want)
			}
		})
	}
}

// PastDay ignore past day
func PastDay(current time.Time) bool {
	now := time.Now()
	diff := now.Sub(current)

	if int(diff.Hours()/24) > 0 {
		return true
	}

	return false
}
