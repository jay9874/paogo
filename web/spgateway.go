package web

import (
	"encoding/json"
	"graduate-project/config"
	"graduate-project/model"
	"net/http"
	"strings"

	spgateway "github.com/appleboy/go-spgateway"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// SPMessage 交易支付系統回傳參數說明
type SPMessage struct {
	Status  string `binding:"required" form:"Status" json:"Status"`
	Message string `binding:"required" form:"Message" json:"Message"`
	Result  string `binding:"required" form:"Result" json:"Result"`
}

// SPResult 回傳資料
type SPResult struct {
	MerchantID      string `form:"MerchantID" json:"MerchantID"`
	Amt             int    `form:"Amt" json:"Amt"`
	TradeNo         string `form:"TradeNo" json:"TradeNo"`
	MerchantOrderNo string `form:"MerchantOrderNo" json:"MerchantOrderNo"`
	PaymentType     string `form:"PaymentType" json:"PaymentType"`
	RespondType     string `form:"RespondType" json:"RespondType"`
	CheckCode       string `form:"CheckCode" json:"CheckCode"`
	PayTime         string `form:"PayTime" json:"PayTime"`
	IP              string `form:"IP" json:"IP"`
	EscrowBank      string `form:"EscrowBank" json:"EscrowBank"`
	CodeNo          string `form:"CodeNo" json:"CodeNo"`
}

// SPResponse 智付寶訊息格式
type SPResponse struct {
	Status  string
	Message string
	Result  SPResult
}

func getSPResponse(c *gin.Context) (SPResponse, error) {
	var data SPMessage
	var result SPResult
	JSONData := c.PostForm("JSONData")

	err := json.Unmarshal([]byte(JSONData), &data)
	if err != nil {
		return SPResponse{}, err
	}

	err = json.Unmarshal([]byte(data.Result), &result)
	if err != nil {
		return SPResponse{}, err
	}

	return SPResponse{
		Status:  data.Status,
		Message: data.Message,
		Result:  result,
	}, nil
}

func Customer(c *gin.Context) {
	response, err := getSPResponse(c)

	if err != nil {
		logrus.Println("error")
		return
	}

	if strings.ToLower(response.Status) == "success" {
		sp := spgateway.New(spgateway.Config{
			MerchantID: config.Spgateway.MerchantID,
			HashKey:    config.Spgateway.HashKey,
			HashIV:     config.Spgateway.HashIV,
		})

		orderCheckCode := sp.OrderCheckCode(spgateway.OrderCheckCode{
			Amt:             response.Result.Amt,
			MerchantOrderNo: response.Result.MerchantOrderNo,
			MerchantID:      response.Result.MerchantID,
			TradeNo:         response.Result.TradeNo,
		})
		if orderCheckCode == response.Result.CheckCode {
			userCash, err := model.DB.QueryCashByOrderNo(response.Result.MerchantOrderNo)
			if err != nil {
				return
			}
			_, err = model.DB.UpdateCashByOrderNo(response.Result.MerchantOrderNo, map[string]interface{}{
				"status": 1,
			})

			if err != nil {
				return
			}

			user, err := model.DB.QueryUserByStudentID(userCash.StudentID)

			err = model.DB.UpdateUser(user.ID, map[string]interface{}{
				"credits": user.Credits + response.Result.Amt,
			})

			/*wjwaddress, err := getWallet("WJW")

			if err != nil {
				return
			}

			useraddress, err := getWallet(user.StudentID)

			if err != nil {
				return
			}

			Blockchain.Send(wjwaddress, useraddress, response.Result.Amt, "3001", true)*/

		}
	}

	c.HTML(
		http.StatusOK,
		"user/cash.html",
		gin.H{},
	)
}

/*func getWallet(bucket string) (wallet string, err error) {

	vaultClient := &Vault.Request{}
	vaultClient.Method = "GET"
	vaultClient.URL, err = url.Parse("http://127.0.0.1:8200/v1/secret/" + bucket)
	if err != nil {
		return "", err
	}
	vaultClient.ClientToken = config.Vault.Token
	req, err := vaultClient.ToHTTP()
	if err != nil {

		return "", err
	}
	client := &http.Client{}
	var resultVault *Vault.Response
	resp, err := client.Do(req)
	if resp != nil {
		resultVault = &Vault.Response{Response: resp}
	}
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	var result map[string]interface{}
	err = resultVault.DecodeJSON(&result)
	if err != nil {

		return "", err
	}
	vaultInfo := result["data"].(map[string]interface{})

	if err != nil {
		return "", err
	}

	return vaultInfo["Wallet"].(string), nil

}*/
