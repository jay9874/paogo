package web

import (
	"fmt"
	"time"
)

func parseTimeStrict(layout, value string) (time.Time, error) {
	t, err := time.Parse(layout, value)
	if err != nil {
		return t, err
	}
	if t.Format(layout) != value {
		return t, fmt.Errorf("invalid time: %q", value)
	}
	return t, nil
}
