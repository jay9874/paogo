package Blockchain

import (
	"bytes"
	"encoding/gob"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Block represents a block in the blockchain
type Block struct {
	Timestamp     int64
	Transactions  []*Transaction
	PrevBlockHash []byte
	Hash          []byte
	Nonce         int
	Height        int
}

// NewBlock creates and returns Block
func NewBlock(transactions []*Transaction, prevBlockHash []byte, height int) *Block {
	block := &Block{time.Now().Unix(), transactions, prevBlockHash, []byte{}, 0, height}
	pow := NewProofOfWork(block)
	nonce, hash := pow.Run()

	block.Hash = hash[:]
	block.Nonce = nonce

	return block
}

// NewGenesisBlock creates and returns genesis Block
func NewGenesisBlock(coinbase *Transaction) *Block {
	return NewBlock([]*Transaction{coinbase}, []byte{}, 0)
}

// HashTransactions returns a hash of the transactions in the block
func (b *Block) HashTransactions() []byte {
	var transactions [][]byte

	for _, tx := range b.Transactions {
		transactions = append(transactions, tx.Serialize())
	}
	mTree := NewMerkleTree(transactions)

	return mTree.RootNode.Data
}

// Serialize serializes the block
func (b *Block) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(b)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

// DeserializeBlock deserializes a block
func DeserializeBlock(d []byte) *Block {
	var block Block

	decoder := gob.NewDecoder(bytes.NewReader(d))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}

	return &block
}

func TestBlock(c *gin.Context) {
	token := c.Param("token")
	/*wallets, _ := NewWallets("3001")
	address := wallets.CreateWallet()
	wallets.SaveToFile("3001")

	fmt.Printf("Your new address: %s\n", address)

	bc := CreateBlockchain(address, "3001")
	defer bc.db.Close()
	UTXOSet := UTXOSet{bc}
	UTXOSet.Reindex()

	fmt.Println("Done!")*/
	_ = GetBalance(token, "3001")
	/*send("18CDp9ExHmgjuGj1mAiiySRoSfpU2pm6Xc", "13eD6SVTR8mahwrjrwPzmNJMmu1yojHauN", 6, "3001", true)

	getBalance("18CDp9ExHmgjuGj1mAiiySRoSfpU2pm6Xc", "3001")
	getBalance("13eD6SVTR8mahwrjrwPzmNJMmu1yojHauN", "3001")*/

	//bc.AddBlock("Jay Send 1 PGC to SC")
	//bc.AddBlock("Jay 2 more BTC to SC")

	//bc.printChain()
	c.JSON(
		http.StatusOK,
		gin.H{},
	)
}
