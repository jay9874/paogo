package web

import (
	"fmt"
)

var (
	errBadRequest        = GpErr{Code: 101, Message: "Bad Input Request"}
	errUserExists        = GpErr{Code: 102, Message: "User Already Exists"}
	errWrongTimeFormat   = GpErr{Code: 103, Message: "Wrong Time Format"}
	errWrongStringFormat = GpErr{Code: 104, Message: "Wrong Sting To Int"}

	errUserCreate       = GpErr{Code: 201, Message: "User Create Error"}
	errStoreCreate      = GpErr{Code: 202, Message: "Store Create Error"}
	errAesCreate        = GpErr{Code: 203, Message: "Aes Create Error"}
	errImgFolder        = GpErr{Code: 204, Message: "Create Image Folder Error"}
	errImgFile          = GpErr{Code: 205, Message: "Create Image File Error"}
	errUserStatusUpdate = GpErr{Code: 206, Message: "User Status Update Error"}
	errUserLogin        = GpErr{Code: 207, Message: "User Login Error"}
	errUserStatus       = GpErr{Code: 208, Message: "User Status error"}
	errUserNotFound     = GpErr{Code: 209, Message: "User Not Found"}
	errStoreNotFound    = GpErr{Code: 210, Message: "Store Not Found"}
	errCreditsEnough    = GpErr{Code: 211, Message: "Credits Not Enough"}
	errUserUpdate       = GpErr{Code: 212, Message: "User Update Error"}
	errOrderCreate      = GpErr{Code: 213, Message: "Create Order Error"}
	errOrderExists      = GpErr{Code: 214, Message: "Order already Exists"}
	errOrderAccept      = GpErr{Code: 215, Message: "Can't accept own order"}
	//special
	errOpenSSl = GpErr{Code: 301, Message: "OpenSSl Can't Found"}
	errVault   = GpErr{Code: 302, Message: "Occured By Vault"}
	//jwt
	errJwtCreate  = GpErr{Code: 401, Message: "Jwt Token Create Error"}
	errJwtInvalid = GpErr{Code: 402, Message: "Jwt Invalid"}
	//redis
	errRedisToken = GpErr{Code: 601, Message: "Redis Token Error Or Expired"}
	// recaptCha
	errRecaptcha = GpErr{Code: 801, Message: "Recaptcha error"}
)

type GpErr struct {
	Code    int
	Message string
}

func (e GpErr) Error() string {
	return fmt.Sprintf("Error Code %d, Error Message %s", e.Code, e.Message)
}
