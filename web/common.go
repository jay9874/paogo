package web

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func errorJson(c *gin.Context, code int, err GpErr) {
	logrus.Errorln(err.Error())

	c.AbortWithStatusJSON(
		code,
		gin.H{
			"code":  err.Code,
			"error": err.Error(),
		},
	)
}
