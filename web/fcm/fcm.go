package fcm

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
)

const (
	// DefaultEndpoint contains endpoint URL of FCM service.
	DefaultEndpoint = "https://fcm.googleapis.com/fcm/send"
)

type FCMClient struct {
	apiKey   string
	client   *http.Client
	endpoint string
}

type FCMResponse struct {
	MulticastID  int64       `json:"multicast_id"`
	Success      int         `json:"success"`
	Failure      int         `json:"failure"`
	CanonicalIDs int         `json:"canonical_ids"`
	Results      []FCMResult `json:"results"`
}

// Result represents the status of a processed message.
type FCMResult struct {
	MessageID      string `json:"message_id"`
	RegistrationID string `json:"registration_id"`
	Error          error  `json:"error"`
}

type Notification struct {
	Title        string `json:"title,omitempty"`
	Body         string `json:"body,omitempty"`
	Icon         string `json:"icon,omitempty"`
	Sound        string `json:"sound,omitempty"`
	Badge        string `json:"badge,omitempty"`
	Tag          string `json:"tag,omitempty"`
	Color        string `json:"color,omitempty"`
	ClickAction  string `json:"click_action,omitempty"`
	BodyLocKey   string `json:"body_loc_key,omitempty"`
	BodyLocArgs  string `json:"body_loc_args,omitempty"`
	TitleLocKey  string `json:"title_loc_key,omitempty"`
	TitleLocArgs string `json:"title_loc_args,omitempty"`
}

type Message struct {
	To                       string                 `json:"to,omitempty"`
	RegistrationIDs          []string               `json:"registration_ids,omitempty"`
	Condition                string                 `json:"condition,omitempty"`
	CollapseKey              string                 `json:"collapse_key,omitempty"`
	Priority                 string                 `json:"priority,omitempty"`
	ContentAvailable         bool                   `json:"content_available,omitempty"`
	DelayWhileIdle           bool                   `json:"delay_while_idle,omitempty"`
	TimeToLive               *uint                  `json:"time_to_live,omitempty"`
	DeliveryReceiptRequested bool                   `json:"delivery_receipt_requested,omitempty"`
	DryRun                   bool                   `json:"dry_run,omitempty"`
	RestrictedPackageName    string                 `json:"restricted_package_name,omitempty"`
	Notification             *Notification          `json:"notification,omitempty"`
	Data                     map[string]interface{} `json:"data,omitempty"`
}

func NewFCMClient(apiKey string) (*FCMClient, error) {
	if apiKey == "" {
		return nil, errors.New("API key can't null")
	}
	c := &FCMClient{
		apiKey:   apiKey,
		endpoint: DefaultEndpoint,
		client:   &http.Client{},
	}

	return c, nil
}

func (c *FCMClient) Send(msg *Message) (*FCMResponse, error) {

	// marshal message
	data, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}

	return c.send(data)
}

// send sends a request.
func (c *FCMClient) send(data []byte) (*FCMResponse, error) {
	// create request
	req, err := http.NewRequest("POST", c.endpoint, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}

	// add headers
	req.Header.Add("Authorization", fmt.Sprintf("key=%s", c.apiKey))
	req.Header.Add("Content-Type", "application/json")

	// execute request
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, errors.New("Request error")
	}
	defer resp.Body.Close()

	// check response status
	if resp.StatusCode != http.StatusOK {
		logrus.Println(resp)
		return nil, errors.New("Response error")
	}

	// build return
	response := new(FCMResponse)
	if err := json.NewDecoder(resp.Body).Decode(response); err != nil {
		return nil, err
	}

	return response, nil
}
