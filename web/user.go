package web

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"graduate-project/config"
	"graduate-project/helper"
	"graduate-project/model"
	"graduate-project/router/module/socket"
	"graduate-project/router/module/storage"
	"graduate-project/web/fcm"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/mail"
	"net/smtp"
	"net/url"
	"strconv"
	"strings"
	"time"

	spgateway "github.com/appleboy/go-spgateway"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/minio/minio-go"
	"github.com/rs/xid"
	"github.com/scorredoira/email"
	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

type registerForm struct {
	Name        string `form:"studentName" binding:"required"`
	StudentID   string `form:"studentId" binding:"required"`
	Password    string `form:"password" binding:"required"`
	Gender      string `form:"gender" binding:"required"`
	Email       string `form:"email" binding:"required"`
	LineID      string `form:"lineId" binding:"required"`
	Phone       string `form:"phone" binding:"required"`
	Development string `form:"development" binding:"required"`
}
type missionForm struct {
	Title     string `form:"title" binding:"required"`
	Commodity string `form:"commodity" binding:"required"`
	Shopname  string `form:"shopname" binding:"required"`
	Runfee    string `form:"runfee" binding:"required"`
	Place     string `form:"place" binding:"required"`
	DeadLine  string `form:"deadline" binding:"required"`
	Remarks   string `form:"remarks"`
}
type cashForm struct {
	Amt   int    `json:"amt" binding:"required"`
	Email string `json:"email" binding:"required"`
}
type userDetail struct {
	SendSMS string `json:"sendSMS" binding:"required"`
}
type jwtformat struct {
	StudentID string `json:"studentId"`
	jwt.StandardClaims
}
type SPgateway struct {
	MerchantID      string
	RespondType     string
	TimeStamp       string
	Version         string
	MerchantOrderNo string
	Amt             int
	ItemDesc        string
}

const (
	privKeyPath = "./userpem/app.rsa"
	pubKeyPath  = "./userpem/app.rsa.pub"
)

var commonIV = []byte{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f}

func hashSha256(str string) string {
	return strings.ToUpper(fmt.Sprintf("%x", sha256.Sum256([]byte(str)))) + randSeq(7)
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func UserRegister(c *gin.Context) {

	c.HTML(
		http.StatusOK,
		"user/register.html",
		gin.H{
			"API": config.Server.Host + "/user/create",
		},
	)
}

func UserLoginView(c *gin.Context) {
	helper.Logger.WithFields(logrus.Fields{
		"animal": "walrus",
		"size":   10,
	}).Info("A group of walrus emerges from the ocean")
	c.HTML(
		http.StatusOK,
		"user/login.html",
		gin.H{
			"API": config.Server.Host + "/user/login",
		},
	)
}

func UserDetailView(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"user/user.html",
		gin.H{},
	)
}

func CreateStoreView(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"user/cshop.html",
		gin.H{},
	)
}

func UserDetail(c *gin.Context) {
	studentID := c.MustGet("userStudentID").(string)

	user, err := model.DB.QueryUserByStudentID(studentID)

	if err != nil {
		errorJson(c, http.StatusBadRequest, errUserNotFound)
		return
	}

	// Move some data to vault
	/*vaultClient := &Vault.Request{}
	vaultClient.Method = "GET"
	vaultClient.URL, err = url.Parse("http://127.0.0.1:8200/v1/secret/" + user.StudentID)
	if err != nil {
		return
	}
	vaultClient.ClientToken = config.Vault.Token
	req, err := vaultClient.ToHTTP()
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errVault)
		return
	}
	client := &http.Client{}
	var resultVault *Vault.Response
	resp, err := client.Do(req)
	if resp != nil {
		resultVault = &Vault.Response{Response: resp}
	}
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errVault)
		return
	}

	defer resp.Body.Close()

	var result map[string]interface{}
	err = resultVault.DecodeJSON(&result)
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errVault)
		return
	}
	vaultInfo := result["data"].(map[string]interface{})
	credits := Blockchain.GetBalance(vaultInfo["Wallet"].(string), "3001")*/
	width := "300"
	height := "300"
	op := "resize"
	c.JSON(
		http.StatusOK,
		gin.H{
			"UserName":    user.Name,
			"StudentID":   user.StudentID,
			"LineID":      user.LineID,
			"Development": user.Development,
			"Email":       user.Email,
			"Wallet":      randSeq(20),
			"SMS":         user.SendSMS,
			"Credits":     user.Credits,
			"Image": "https://paogo.tk/display?url=" +
				storage.S3.GetFile(config.Minio.Bucket, user.ProfileImage) + "&w=" + width + "&h=" +
				height + "&op=" + op + "&upscale=0",
		},
	)
}

func CreateUser(c *gin.Context) {
	logrus.Println(config.Redis.Addr)
	var form registerForm

	if err := c.ShouldBind(&form); err != nil {
		logrus.Println(err)
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}

	/*if strings.Contains(c.Request.UserAgent(), "Mobile") != true && strings.Contains(c.Request.UserAgent(), "Android") != true && strings.Contains(c.Request.UserAgent(), "iPhone") != true {
		re := recaptcha.R{
			Secret: "6LfC3T4UAAAAAKyReapPzg7jpCBuFfZcBwdbXKvg",
		}
		isValid := re.Verify(*c.Request)
		if !isValid {
			for _, msg := range re.LastError() {
				logrus.Debugf("%s\n", msg)
			}
			errorJson(c, http.StatusBadRequest, errRecaptcha)
			return
		}
	}*/

	file, err := c.FormFile("profile_image")
	if err != nil {
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}

	if _, err := model.DB.QueryUserByStudentID(strings.ToUpper(form.StudentID)); err == nil {
		errorJson(c, http.StatusBadRequest, errUserExists)
		return
	}

	guid := xid.New()
	objectName := guid.String() + ".jpg"
	filePath := storage.S3.FilePath(objectName)
	err = c.SaveUploadedFile(file, "./upload/user/"+objectName)
	go func() {
		// minio upload
		src, err := file.Open()
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("open file err: %s", err.Error()))
			return
		}
		defer src.Close()

		data, err := ioutil.ReadAll(src)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("read file err: %s", err.Error()))
			return
		}

		if err := storage.S3.UploadFile(
			config.Minio.Bucket,
			objectName,
			filePath,
			data,
			"image/jpg",
		); err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
			return
		}
	}()

	if err != nil {
		errorJson(c, http.StatusInternalServerError, errImgFile)
		return
	}

	hash, err := aes.NewCipher([]byte(config.User.HashKey))
	if err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusInternalServerError, errAesCreate)
		return
	}
	//encode
	cfb := cipher.NewCFBEncrypter(hash, commonIV)
	ciphertext := make([]byte, len(c.PostForm("password")))
	cfb.XORKeyStream(ciphertext, []byte(form.Password))
	logrus.Debugf("%s=>%x\n", c.PostForm("password"), ciphertext)

	user := model.NewUser()
	user.ID = bson.NewObjectId()
	user.Name = c.PostForm("studentName")
	user.StudentID = strings.ToUpper(c.PostForm("studentId"))
	user.Password = fmt.Sprintf("%x", ciphertext)
	user.Gender, _ = strconv.Atoi(c.PostForm("gender"))
	user.Email = c.PostForm("email")
	user.LineID = c.PostForm("lineId")
	user.ProfileImage = guid.String() + ".jpg"
	user.Development = c.PostForm("development")
	user.Credits = 50
	user.Phone = c.PostForm("phone")
	user.SendSMS = "False"

	token := hashSha256(user.StudentID)

	var client = ConnectRedis()
	err = client.Set(token, user.ID.Hex(), 60*time.Minute).Err()

	if err != nil {
		logrus.Debug("error", err)
	}

	err = model.DB.CreateUser(user)

	if err != nil {
		errorJson(c, http.StatusInternalServerError, errUserCreate)
		return
	}
	content := TextRegisterEmail(token)
	go func(message string, studentID string) {
		err = SendEmailToUser(message, studentID)
		if err != nil {
			logrus.Debugf("Send Email Error %s", err.Error())
		}

		/*wallets, _ := Blockchain.NewWallets("3001")
		address := wallets.CreateWallet()
		wallets.SaveToFile("3001")

		// Move some data to vault
		vaultClient := &Vault.Request{}
		vaultClient.Method = "POST"
		vaultClient.URL, err = url.Parse("http://127.0.0.1:8200/v1/secret/" + user.StudentID)
		if err != nil {
			return
		}
		vaultClient.ClientToken = config.Vault.Token
		vaultClient.SetJSONBody(map[string]interface{}{
			"Phone":  user.Phone,
			"Line":   user.LineID,
			"Email":  user.Email,
			"Wallet": address,
		})
		req, err := vaultClient.ToHTTP()
		if err != nil {
			errorJson(c, http.StatusInternalServerError, errVault)
			return
		}
		client := &http.Client{}
		_, err = client.Do(req)

		if err != nil {
			errorJson(c, http.StatusInternalServerError, errVault)
			return
		}*/
	}(content, user.StudentID)

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "ok",
		},
	)
}

func UserLogin(c *gin.Context) {
	var SignKey []byte
	studentId := strings.ToUpper(c.PostForm("studentId"))
	password := c.PostForm("password")

	go func() {
		var client = ConnectRedis()
		defer client.Close()
		if v := c.PostForm("devicetoken"); v != "" {
			if val, err := client.Get("Device-" + studentId).Result(); len(val) > 10 && val != v {

				var FCM *fcm.FCMClient
				if err != nil {
					return
				}
				FCM, err = fcm.NewFCMClient("AAAAFwyBLeQ:APA91bFWvpSiHzWfiDtBUAN61aHLIutgtdkRpiq0dF1ZK8bDBShnXp8Ln9cZcrtqKeWVEZDRJZropXhvbOohsoo25bJGeVhNnw8WgjOR1uQAEpVJocyjAFhX6GuE_eVx_BA3uB6uUDNy")
				if err != nil {
					logrus.Println(err)
					return
				}

				message := &fcm.Message{
					To: val,
				}

				message.Priority = "high"
				message.Data = map[string]interface{}{
					"Notification": "Other side login",
					"IP":           c.ClientIP(),
				}

				test, err := FCM.Send(message)
				if err != nil {
					logrus.Println(err)
					logrus.Println(test)
				}

				_, err = client.Del(val).Result()
				if err != nil {
					logrus.Println(err)
				}

				err = client.Set("Device-"+studentId, v, 0).Err()

				if err != nil {
					logrus.Debug("error", err)
				}

			} else {
				err := client.Set("Device-"+studentId, v, 0).Err()

				if err != nil {
					logrus.Debug("error", err)
				}
			}

		}
	}()

	user, err := model.DB.QueryUserByStudentID(studentId)
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errUserLogin)
		return
	}
	if user.Status != 1 {
		errorJson(c, http.StatusBadGateway, errUserStatus)
		return
	}
	hash, err := aes.NewCipher([]byte(config.User.HashKey))
	if err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusInternalServerError, errAesCreate)
		return
	}
	//decode from post data
	cfb := cipher.NewCFBEncrypter(hash, commonIV)
	ciphertext := make([]byte, len(password))
	cfb.XORKeyStream(ciphertext, []byte(password))

	if user.StudentID != studentId || fmt.Sprintf("%x", ciphertext) != user.Password {
		errorJson(c, http.StatusForbidden, errUserLogin)
		return
	}
	token := jwt.New(jwt.GetSigningMethod("HS256"))

	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.StudentID
	claims["exp"] = time.Now().Add(time.Hour).Unix()
	claims["orig_iat"] = time.Now().Unix()

	SignKey, err = ioutil.ReadFile(privKeyPath)
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errOpenSSl)
		return
	}

	tokenstr, err := token.SignedString(SignKey)
	if err != nil {
		errorJson(c, http.StatusInternalServerError, errJwtCreate)
		return
	}
	logrus.Println("success:" + tokenstr)
	c.Writer.Header().Set("Authorization", "Bearer "+tokenstr)
	c.JSON(
		http.StatusOK,
		gin.H{
			"Status":  "Success",
			"Coin":    user.Credits,
			"Name":    user.Name,
			"Picture": user.ProfileImage,
		},
	)
}

func CreateStore(c *gin.Context) {

	var form missionForm
	userStudentID := c.MustGet("userStudentID").(string)
	if err := c.Bind(&form); err != nil {
		logrus.Println(err)
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}

	var deadLine time.Time

	deadLine, err := parseTimeStrict("2006-01-02 15:04", form.DeadLine)
	if err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusBadRequest, errWrongTimeFormat)
		return
	}

	runfee, err := strconv.Atoi(form.Runfee)
	if err != nil {
		errorJson(c, http.StatusBadRequest, errWrongStringFormat)
		return
	}

	if user, err := model.DB.QueryUserByStudentID(userStudentID); err == nil {
		if user.Credits < runfee {
			errorJson(c, http.StatusForbidden, errCreditsEnough)
			return
		}
	} else {
		errorJson(c, http.StatusInternalServerError, errUserNotFound)
		return
	}
	publicID := userStudentID[0:2] + time.Now().Format("060102150405") + userStudentID[3:]

	mission := model.Mission{
		ID:        bson.NewObjectId(),
		StudentID: userStudentID,
		Commodity: form.Commodity,
		Title:     form.Title,
		Shopname:  form.Shopname,
		PublicID:  publicID,
		Place:     form.Place,
		Runfee:    runfee,
		DeadLine:  deadLine,
		Remarks:   form.Remarks,
		CreatedAt: time.Now(),
	}

	err = model.DB.CreateMission(&mission)

	if err != nil {
		logrus.Println(err)
		errorJson(c, http.StatusInternalServerError, errStoreCreate)
		return
	}

	go func(mission model.Mission) {
		shopimage, err := c.MultipartForm()
		if err != nil {
			logrus.Println("part form", err.Error())
			errorJson(c, http.StatusBadRequest, errBadRequest)
			return
		}
		files := shopimage.File["shopimage[]"]
		var filenames []string
		for i, file := range files {
			filename := hashSha256(userStudentID + fmt.Sprintf("%c", userStudentID[i]) + strconv.FormatInt(time.Now().Unix(), 10))
			if err := c.SaveUploadedFile(file, "./upload/store/"+filename+".jpg"); err != nil {
				logrus.Println(err)
				continue
			}

			// minio upload
			src, err := file.Open()
			if err != nil {
				c.String(http.StatusBadRequest, fmt.Sprintf("open file err: %s", err.Error()))
				continue
			}
			defer src.Close()

			data, err := ioutil.ReadAll(src)
			if err != nil {
				c.String(http.StatusBadRequest, fmt.Sprintf("read file err: %s", err.Error()))
				continue
			}

			if err := storage.S3.UploadFile(
				config.Minio.Bucket,
				filename+".jpg",
				storage.S3.FilePath(filename),
				data,
				"image/jpg",
			); err != nil {
				c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
				continue
			}
			filenames = append(filenames, filename+".jpg")
		}

		_, err = model.DB.UpdateStore(mission.ID, map[string]interface{}{
			"shopimage": filenames,
		})

		if err != nil {
			logrus.Println("Create store image " + mission.ID + "failed")
		}
	}(mission)
	go func() {

		var client *fcm.FCMClient
		storeAmount, err := model.DB.CountAllMission()
		if err != nil {
			return
		}
		client, err = fcm.NewFCMClient("AAAAFwyBLeQ:APA91bFWvpSiHzWfiDtBUAN61aHLIutgtdkRpiq0dF1ZK8bDBShnXp8Ln9cZcrtqKeWVEZDRJZropXhvbOohsoo25bJGeVhNnw8WgjOR1uQAEpVJocyjAFhX6GuE_eVx_BA3uB6uUDNy")
		if err != nil {
			logrus.Println(err)
			return
		}
		message := &fcm.Message{
			To: "/topics/ShopCount",
		}

		message.Priority = "high"
		message.Data = map[string]interface{}{
			"Amount": storeAmount,
		}

		test, err := client.Send(message)
		if err != nil {
			logrus.Println(err)
			logrus.Println(test)
		}
	}()

	// twilio
	go func(userStudentID string, missionID bson.ObjectId) {
		user, err := model.DB.QueryUserByStudentID(userStudentID)
		if err != nil || user.Credits <= 0 {
			return
		}
		if user.SendSMS == "True" && config.SMS.Open {
			// Set account keys & information
			accountSid := "AC1ce44a1e8f6e9ba4366d9dc93060f101"
			authToken := "5f817c02e05820ebb6b202b987b7b7ab"
			urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json"

			// Create possible message bodies
			quotes := user.Name + ",感謝您於Paogo平台建立訂單,祝您有個愉快的一天。(系統簡訊勿回)"

			userPhone := user.Phone[1:]
			logrus.Println(userPhone)
			msgData := url.Values{}
			msgData.Set("To", "+886"+userPhone)
			msgData.Set("From", "+14807251398")
			msgData.Set("Body", quotes)
			msgDataReader := *strings.NewReader(msgData.Encode())

			client := &http.Client{}
			req, _ := http.NewRequest("POST", urlStr, &msgDataReader)
			req.SetBasicAuth(accountSid, authToken)
			req.Header.Add("Accept", "application/json")
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

			resp, _ := client.Do(req)
			log := &model.SMS{
				ID:          bson.NewObjectId(),
				StudentID:   user.StudentID,
				MissionID:   missionID,
				PhoneNumber: user.Phone,
				CreatedAt:   time.Now(),
			}
			if resp.StatusCode >= 200 && resp.StatusCode < 300 {
				var data map[string]interface{}
				decoder := json.NewDecoder(resp.Body)
				err := decoder.Decode(&data)
				if err == nil {
					log.TwilioID = data["sid"].(string)
					err := model.DB.UpdateUser(user.ID, map[string]interface{}{
						"credits": user.Credits - 1,
					})
					if err != nil {
						log.Status = 1
					} else {
						log.Status = 2
					}
					if err := model.DB.CreateSMS(log); err != nil {
						logrus.Println(err)
						return
					}
				}
			} else {
				log.Status = 0
				if err := model.DB.CreateSMS(log); err != nil {
					logrus.Println(err)
					return
				}
			}
		}
	}(userStudentID, mission.ID)

	// socket boardcast to user
	go socket.NewMission(mission.PublicID)

	c.JSON(
		http.StatusOK,
		gin.H{
			"Message": "ok",
		},
	)
}

func GetAllStore(c *gin.Context) {
	stores, err := model.DB.GetAllMission()
	if err != nil {
		errorJson(c, http.StatusNotFound, errStoreNotFound)
		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": stores,
		},
	)
}

func GetStoreDetail(c *gin.Context) {
	userStudentID := c.MustGet("userStudentID").(string)
	store, _ := model.DB.QueryMission(userStudentID)

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": store,
		},
	)
}

func VerifyRegister(c *gin.Context) {
	token := c.Param("token")
	var client = ConnectRedis()
	//get redis key
	id, err := client.Get(token).Result()
	if err != nil {
		errorJson(c, http.StatusBadGateway, errRedisToken)
		return
	}
	logrus.Debugf("key:%s value:%s", token, id)
	err = model.DB.UpdateUser(bson.ObjectIdHex(id), map[string]interface{}{
		"status": 1,
	})
	if err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusInternalServerError, errUserStatusUpdate)
		return
	}

	_, err = client.Del(token).Result()
	if err != nil {
		logrus.Println(err)
	}
	c.HTML(
		http.StatusOK,
		"user/verifySuccess.html",
		gin.H{},
	)
}

func StoreCash(c *gin.Context) {
	/*data := SPgateway{
		MerchantID:      "3430112",
		RespondType:     "JSON",
		TimeStamp:       "1485232229",
		Version:         "1.4",
		MerchantOrderNo: "S_1485232229",
		Amt:             40,
		ItemDesc:        "UnitTest",
	}*/

	c.HTML(
		http.StatusOK,
		"user/recharge.html",
		gin.H{
			"Host":       config.Server.Host,
			"API":        config.Spgateway.API,
			"NotifyURL":  config.Server.Host + "/spgateway/customer",
			"MerchantID": config.Spgateway.MerchantID,
		},
	)
}

func CreateOrder(c *gin.Context) {
	userStudentID := c.MustGet("userStudentID").(string)

	row := &model.Order{
		ID:        bson.NewObjectId(),
		StudentID: userStudentID,
		MissionID: c.PostForm("publicID"),
		CreatedAt: time.Now(),
	}
	if mission, err := model.DB.QueryMissionByPublicID(row.MissionID); err == nil && mission.StudentID != "" {
		if row.StudentID == mission.StudentID {
			errorJson(c, http.StatusForbidden, errOrderAccept)
			return
		}
	}
	if order, err := model.DB.QueryOrderByPublicID(row.MissionID); err == nil && len(order) > 0 {
		errorJson(c, http.StatusInternalServerError, errOrderExists)
		return
	}
	if err := model.DB.CreateOrder(row); err != nil {
		errorJson(c, http.StatusInternalServerError, errOrderCreate)
		return
	}

	go socket.AcceptOrder(c.PostForm("publicID"))

	go func() {
		mission, err := model.DB.QueryMissionByPublicID(row.MissionID)

		if err != nil {
			logrus.Println(err)
			return
		}

		packer, err := model.DB.QueryUserByStudentID(userStudentID)

		if err != nil {
			logrus.Println(err)
			return
		}

		var client = ConnectRedis()
		defer client.Close()
		if val, err := client.Get("Device-" + mission.StudentID).Result(); err != redis.Nil {

			var FCM *fcm.FCMClient
			if err != nil {
				logrus.Println(err)
				return
			}
			FCM, err = fcm.NewFCMClient("AAAAFwyBLeQ:APA91bFWvpSiHzWfiDtBUAN61aHLIutgtdkRpiq0dF1ZK8bDBShnXp8Ln9cZcrtqKeWVEZDRJZropXhvbOohsoo25bJGeVhNnw8WgjOR1uQAEpVJocyjAFhX6GuE_eVx_BA3uB6uUDNy")
			if err != nil {
				logrus.Println(err)
				return
			}

			message := &fcm.Message{
				To: val,
			}
			notification := &fcm.Notification{
				Title: "Paogo消息通知",
				Body:  packer.Name + "接受了您的訂單",
				Sound: "default",
			}
			message.Priority = "high"

			message.Notification = notification

			test, err := FCM.Send(message)
			if err != nil {
				logrus.Println(err)
				logrus.Println(test)
			}
		}
	}()

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "ok",
		},
	)
}

func GetUserTask(c *gin.Context) {
	userStudentID := c.MustGet("userStudentID").(string)

	missions, _ := model.DB.QueryMission(userStudentID)

	orders, _ := model.DB.QueryOrder(userStudentID)

	result := make([]map[string]interface{}, 0)
	for _, v := range missions {
		contactway := ""
		packer, err := model.DB.QueryOrderByPublicID(v.PublicID)

		if len(packer) >= 1 {
			contactway = packer[0].StudentID
		}

		mission := map[string]interface{}{
			"title":     v.Title,
			"shopname":  v.Shopname,
			"creater":   v.StudentID,
			"place":     v.Place,
			"deadline":  v.DeadLine,
			"remarks":   v.Remarks,
			"runfee":    v.Runfee,
			"shopImage": v.ShopImage,
			"Status":    v.Status,
			"publicID":  v.PublicID,
			"contact":   contactway,
		}
		if err != nil || len(packer) == 0 {
			mission["havePacker"] = false
		} else {
			mission["havePacker"] = true
		}
		result = append(result, mission)
	}

	orderList := make([]map[string]interface{}, 0)
	for _, v := range orders {
		missions, err := model.DB.QueryMissionByPublicID(v.MissionID)

		if err == nil {
			mission := map[string]interface{}{
				"title":     missions.Title,
				"shopname":  missions.Shopname,
				"place":     missions.Place,
				"deadline":  missions.DeadLine,
				"remarks":   missions.Remarks,
				"runfee":    missions.Runfee,
				"shopImage": missions.ShopImage,
				"contact":   missions.StudentID,
				"publicID":  missions.PublicID,
				"status":    missions.Status,
			}

			orderList = append(orderList, mission)
		}

	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"Missions": result,
			"Orders":   orderList,
		},
	)
}

func CashOrder(c *gin.Context) {
	var json cashForm

	if err := c.ShouldBindJSON(&json); err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}
	StudentID := c.MustGet("userStudentID").(string)

	if StudentID == "" {
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}
	guid := xid.New()

	row := &model.UserCash{
		ID:        bson.NewObjectId(),
		StudentID: StudentID,
		OrderNumber: fmt.Sprintf(
			"%c%sT%02d",
			StudentID[0],
			strconv.FormatInt(time.Now().Unix(), 10),
			guid.Time().Day(),
		),
		Amt:       json.Amt,
		Email:     json.Email,
		Status:    0,
		CreatedAt: time.Now(),
	}

	if err := model.DB.CreateCashOrder(row); err != nil {
		errorJson(c, http.StatusInternalServerError, errUserStatusUpdate)
		return
	}
	unixTime := time.Now().Unix()

	sp := spgateway.New(spgateway.Config{
		MerchantID: config.Spgateway.MerchantID,
		HashKey:    config.Spgateway.HashKey,
		HashIV:     config.Spgateway.HashIV,
	})
	orderCheckValue := sp.OrderCheckValue(spgateway.OrderCheckValue{
		Amt:             json.Amt,
		MerchantOrderNo: row.OrderNumber,
		MerchantID:      config.Spgateway.MerchantID,
		TimeStamp:       strconv.FormatInt(unixTime, 10),
		Version:         "1.2",
	})

	c.JSON(
		http.StatusOK,
		gin.H{
			"Status":          "Success",
			"MerchantOrderNo": row.OrderNumber,
			"CheckValue":      orderCheckValue,
			"TimeStamp":       time.Now().Unix(),
		},
	)
}

func UpdateUser(c *gin.Context) {
	var detail userDetail
	logrus.Println(c.Request)
	if err := c.ShouldBindJSON(&detail); err != nil {
		logrus.Debugf("%s", err)
		errorJson(c, http.StatusBadRequest, errBadRequest)
		return
	}

	StudentID := c.MustGet("userStudentID").(string)

	user, err := model.DB.QueryUserByStudentID(StudentID)

	if err != nil {
		errorJson(c, http.StatusBadRequest, errUserNotFound)
		return
	}
	logrus.Println("SMS:", detail.SendSMS)
	if detail.SendSMS == "True" {
		if user.Credits <= 0 {
			errorJson(c, http.StatusBadRequest, errCreditsEnough)
			return
		}
	} else {
		detail.SendSMS = "False"
	}
	err = model.DB.UpdateUser(user.ID, map[string]interface{}{
		"sendSMS": detail.SendSMS,
	})

	if err != nil {
		errorJson(c, http.StatusBadRequest, errUserUpdate)
		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "OK",
		},
	)
}

func GetChatHistory(c *gin.Context) {

	//time := 0
	StudentID := c.MustGet("userStudentID").(string)

	members := []string{StudentID, c.PostForm("member")}

	history, err := model.DB.GetChatMsg(members)

	if err != nil {
		logrus.Println(err)
		return
	}

	/*for _, v := range members {
		for _, k := range history.Members {
			if v == k {
				time++
			}
		}
	}

	if time != 2 {
		history = model.Chat{}
	}*/

	c.JSON(
		http.StatusOK,
		gin.H{
			"result": history,
		},
	)
}

func UpdateShopping(c *gin.Context) {
	publicID := c.PostForm("publicID")

	err := model.DB.UpdateMissionByPublicID(publicID, map[string]interface{}{
		"status": 3,
	})

	if err != nil {
		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"Status": "Success",
		},
	)
}

func FinishMission(c *gin.Context) {
	publicID := c.PostForm("publicID")

	err := model.DB.UpdateMissionByPublicID(publicID, map[string]interface{}{
		"status": 4,
	})

	if err != nil {
		return
	}

	go func() {
		createrInfo, err := model.DB.QueryMissionByPublicID(publicID)

		if err != nil {
			return
		}

		creater, err := model.DB.QueryUserByStudentID(createrInfo.StudentID)

		if err != nil {
			return
		}

		err = model.DB.UpdateUser(creater.ID, map[string]interface{}{
			"credits": creater.Credits - createrInfo.Runfee,
		})

		if err != nil {
			return
		}

		packerInfo, err := model.DB.QueryOrderByPublicID(publicID)

		if err != nil {
			return
		}

		packer, err := model.DB.QueryUserByStudentID(packerInfo[0].StudentID)

		if err != nil {
			return
		}

		err = model.DB.UpdateUser(packer.ID, map[string]interface{}{
			"credits": packer.Credits + createrInfo.Runfee,
		})

		if err != nil {
			return
		}

	}()

	c.JSON(
		http.StatusOK,
		gin.H{
			"Status": "Success",
		},
	)
}

func MinioUpload(c *gin.Context) {
	endpoint := "127.0.0.1:8082"
	accessKeyID := "L68B5DKTH3ALCLDT4DAA"
	secretAccessKey := "u8VhxTDAEbuU6kfda/1UropVC8GIlRvtSbiG2YN4"
	useSSL := false

	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
	}

	// Make a new bucket called mymusic.
	bucketName := "practice"
	location := "test2"

	err = minioClient.MakeBucket(bucketName, location)
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, err := minioClient.BucketExists(bucketName)
		if err == nil && exists {
			log.Printf("We already own %s\n", bucketName)
		} else {
			log.Fatalln(err)
		}
	}
	log.Printf("Successfully created %s\n", bucketName)

	// Upload the zip file
	objectName := "practice1.jpg"
	filePath := "./upload/store/7F62F22A4812014644B2B328CB2EA60F68B31AD77CBAFA47A155876A012F9978.jpg"
	contentType := "application/x-jpg"

	// Upload the zip file with FPutObject
	n, err := minioClient.FPutObject(bucketName, objectName, filePath, minio.PutObjectOptions{ContentType: contentType})
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, n)

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "ok",
		},
	)
}

func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	/*hashKey := "12345678901234567890123456789012"
	hashIV := []byte("1234567890123456")
	v, _ := query.Values(&data)

	block, _ := aes.NewCipher([]byte(hashKey))

	plantText := PKCS7Padding([]byte(v.Encode()), block.BlockSize())

	blockModel := cipher.NewCBCEncrypter(block, hashIV)

	ciphertext := make([]byte, len(plantText))

	blockModel.CryptBlocks(ciphertext, plantText)*/
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func TextRegisterEmail(token string) string {

	message := fmt.Sprintf(`感謝您的註冊<br />
							<a href="%s/user/regisert_verification/%s"><h2>請點此連結完成認證</h2></a>`, config.Server.Host, token)
	return message

}
func SendEmailToUser(message string, studentID string) error {
	title := fmt.Sprintf("感謝您的註冊-WJW")

	m := email.NewHTMLMessage(title, message)
	m.From = mail.Address{Name: "WJW Team", Address: "jiejhihjhang@gmail.com"}
	m.To = []string{studentID + "@mailst.cjcu.edu.tw"}

	auth := smtp.PlainAuth("", "jiejhihjhang@gmail.com", "mfsbslqaldijuocn", "smtp.gmail.com")

	if err := email.Send("smtp.gmail.com:587", auth, m); err != nil {
		logrus.Debugln(err)
		return err
	}
	return nil
}

func ConnectRedis() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     config.Redis.Addr,
		Password: "",
		DB:       0,
	})
}
