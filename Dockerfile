FROM alpine:3.7

LABEL maintainer="Jay <jiejhihjhang@gmail.com>"

RUN apk add --no-cache ca-certificates

EXPOSE 7010

ADD bin/graduate-project /

ENTRYPOINT ["/graduate-project"]
CMD ["server"]