EXECUTABLE :=graduate-project
DEVELOP :=develop-GCP
SOURCES ?= $(shell find . -name "*.go" -type f)
PACKAGES ?= $(shell go list ./... | grep -v /vendor/)

DEPLOY_ACCOUNT := jay
DEPLOY_IMAGE := $(EXECUTABLE)

TAGS ?=
LDFLAGS ?= -X 'main.Version=$(VERSION)'
GOFMT ?= gofmt "-s"
GOFILES := $(shell find . -name "*.go" -type f -not -path "./vendor/*")

ifneq ($(shell uname), Darwin)
	EXTLDFLAGS = -extldflags "-static" $(null)
else
	EXTLDFLAGS =
endif

all: build

.PHONY: tar
tar:
	tar -zcvf release.tar.gz bin env Dockerfile Makefile

build: $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES)
	go build -v -o bin/$@ ./cmd

.PHONY: generate
generate:
	@which fileb0x > /dev/null; if [ $$? -ne 0 ]; then \
		go get -u github.com/appleboy/fileb0x; \
	fi
	go generate $(PACKAGES)

.PHONY: check_image
check_image:
	if [ "$(shell sudo docker ps -aq -f name=$(EXECUTABLE))" ]; then \
		sudo docker rm -f $(EXECUTABLE); \
	fi

.PHONY: dev
dev: build_image check_image
	sudo docker run -d --name $(DEPLOY_IMAGE) --env-file env/env.$@ -v /home/um/syslog:/Log -v /home/um/photo:/upload -v /home/um/user-pem:/userpem --network="host" --restart always $(DEPLOY_ACCOUNT)/$(DEPLOY_IMAGE)

develop: $(DEVELOP)

$(DEVELOP): $(SOURCES)
	env GOOS=linux GOARCH=amd64 GOARM=7 go build -v -o develop/$@ ./cmd

.PHONY: fmt
fmt:
	$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check:
	# get all go files and run go fmt on them
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

.PHONY: test-vendor
test-vendor:
	@hash govendor > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go get -u github.com/kardianos/govendor; \
	fi
	govendor list +unused | tee "$(TMPDIR)/wc-gitea-unused"
	[ $$(cat "$(TMPDIR)/wc-gitea-unused" | wc -l) -eq 0 ] || echo "Warning: /!\\ Some vendor are not used /!\\"

	govendor list +outside | tee "$(TMPDIR)/wc-gitea-outside"
	[ $$(cat "$(TMPDIR)/wc-gitea-outside" | wc -l) -eq 0 ] || exit 1

	govendor status || exit 1

embedmd:
	@hash embedmd > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go get -u github.com/campoy/embedmd; \
	fi
	embedmd -d *.md

docker_build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -tags '$(TAGS)' -ldflags "$(EXTLDFLAGS)-s -w $(LDFLAGS)" -o bin/$(EXECUTABLE) ./cmd

build_image:
	sudo docker build -t $(DEPLOY_ACCOUNT)/$(DEPLOY_IMAGE) -f Dockerfile .

.PHONY:goveralls 
goveralls:check_goveralls
	goveralls -service drone.io -repotoken 8KYYMaghslysoyvQn8wg5KciEBtCsxGU4OGzL 

check_goveralls:
	@which goveralls > /dev/null; if [ $$? -ne 0 ]; then \
		go get -u github.com/mattn/goveralls; \
	fi