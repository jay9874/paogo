package main

import (
	"graduate-project/config"
	"os"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/joho/godotenv"
	_ "github.com/joho/godotenv/autoload"
	"gopkg.in/urfave/cli.v2"
)

var Version = "v1.0.0-dev"

func main() {
	if env := os.Getenv("GRADUATE-PROJECT_ENV_FILE"); env != "" {
		godotenv.Load(env)
	}

	app := &cli.App{
		Name:      "garuate-project",
		Usage:     "Home page",
		Copyright: "Copyright (c) 2017 Jay",
		Version:   Version,
		Compiled:  time.Now(),

		Authors: []*cli.Author{
			{
				Name:  "Jay",
				Email: "aawer12345tw@yahoo.com.tw",
			},
		},

		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "debug",
				Value:       false,
				Usage:       "Activate debug information",
				EnvVars:     []string{"GP_DEBUG"},
				Destination: &config.Debug,
				Hidden:      true,
			},
		},

		Before: func(c *cli.Context) error {
			logrus.SetOutput(os.Stdout)

			if config.Debug {
				logrus.SetLevel(logrus.DebugLevel)
			} else {
				logrus.SetLevel(logrus.InfoLevel)
			}

			return nil
		},
		Commands: []*cli.Command{
			Server(),
		},
	}

	cli.HelpFlag = &cli.BoolFlag{
		Name:    "help",
		Aliases: []string{"h"},
		Usage:   "Show the help",
	}

	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"v"},
		Usage:   "Print the current version of that tool",
	}
	if err := app.Run(os.Args); err != nil {
		os.Exit(1)
	}
}
