package main

import (
	"graduate-project/config"
	"net/http"
	"time"

	"graduate-project/router"

	"github.com/sirupsen/logrus"
	cli "gopkg.in/urfave/cli.v2"
)

// Server provides the sub-command to start the API server.
func Server() *cli.Command {
	return &cli.Command{
		Name:  "server",
		Usage: "Start the API",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "db-driver",
				Value:       "mysql",
				Usage:       "Database driver selection",
				EnvVars:     []string{"GP_DB_DRIVER"},
				Destination: &config.Database.Driver,
			},
			&cli.StringFlag{
				Name:        "db-name",
				Value:       "graduate",
				Usage:       "Name for database connection",
				EnvVars:     []string{"GP_DB_NAME"},
				Destination: &config.Database.Name,
			},
			&cli.StringFlag{
				Name:        "db-username",
				Value:       "root",
				Usage:       "Username for database connection",
				EnvVars:     []string{"GP_DB_USERNAME"},
				Destination: &config.Database.Username,
			},
			&cli.StringFlag{
				Name:        "db-password",
				Value:       "root",
				Usage:       "Password for database connection",
				EnvVars:     []string{"GP_DB_PASSWORD"},
				Destination: &config.Database.Password,
			},
			&cli.StringFlag{
				Name:        "root",
				Value:       "/",
				Usage:       "Root folder of the app",
				EnvVars:     []string{"GP_SERVER_ROOT"},
				Destination: &config.Server.Root,
			},
			&cli.StringFlag{
				Name:        "host",
				Value:       "http://localhost:9090",
				Usage:       "External access to server",
				EnvVars:     []string{"GP_SERVER_HOST"},
				Destination: &config.Server.Host,
			},
			&cli.StringFlag{
				Name:        "addr",
				Value:       ":9090",
				Usage:       "Address to bind server",
				EnvVars:     []string{"GP_SERVER_ADDR"},
				Destination: &config.Server.Addr,
			},
			&cli.StringFlag{
				Name:        "assets",
				Value:       "",
				Usage:       "Path to custom assets and templates",
				EnvVars:     []string{"GP_SERVER_ASSETS"},
				Destination: &config.Server.Assets,
			},
			&cli.BoolFlag{
				Name:        "pprof",
				Value:       false,
				Usage:       "Enable pprof debugging server",
				EnvVars:     []string{"GP_SERVER_PPROF"},
				Destination: &config.Server.Pprof,
			},
			&cli.StringFlag{
				Name:        "hashkey",
				Value:       "wjwteam",
				Usage:       "User password hash key",
				EnvVars:     []string{"GP_PWD_SECRET"},
				Destination: &config.User.HashKey,
			},
			&cli.StringFlag{
				Name:        "VerifyKey",
				Value:       "",
				Usage:       "jwt-VerifyKey",
				EnvVars:     []string{"GP_VERIFY_KEY"},
				Destination: &config.JWT.VerifyKey,
			},
			&cli.StringFlag{
				Name:        "SignKey",
				Value:       "",
				Usage:       "jwt-SignKey",
				EnvVars:     []string{"GP_SIGN_KEY"},
				Destination: &config.JWT.SignKey,
			},
			&cli.StringFlag{
				Name:        "spgateway-api",
				Value:       "",
				Usage:       "Spgateway Api",
				EnvVars:     []string{"GP_SPGATEWAY_API"},
				Destination: &config.Spgateway.API,
			},
			&cli.StringFlag{
				Name:        "spgateway-merchant-id",
				Value:       "",
				Usage:       "Spgateway merchant id",
				EnvVars:     []string{"GP_SPGATEWAY_MERCHANT_ID"},
				Destination: &config.Spgateway.MerchantID,
			},
			&cli.StringFlag{
				Name:        "spgateway-hash-key",
				Value:       "",
				Usage:       "Spgateway hash key",
				EnvVars:     []string{"GP_SPGATEWAY_HASH_KEY"},
				Destination: &config.Spgateway.HashKey,
			},
			&cli.StringFlag{
				Name:        "spgateway-hash-iv",
				Value:       "",
				Usage:       "Spgateway HashIV",
				EnvVars:     []string{"GP_SPGATEWAY_HASH_IV"},
				Destination: &config.Spgateway.HashIV,
			},
			&cli.StringFlag{
				Name:        "minio-access-id",
				Usage:       "minio-access-id",
				EnvVars:     []string{"GP_MINIO_ACCESS_ID"},
				Destination: &config.Minio.AccessID,
			},
			&cli.StringFlag{
				Name:        "minio-secret-key",
				Usage:       "minio-secret-key",
				EnvVars:     []string{"GP_MINIO_SECRET_KEY"},
				Destination: &config.Minio.SecretKey,
			},
			&cli.StringFlag{
				Name:        "minio-endpoint",
				Usage:       "minio-endpoint",
				EnvVars:     []string{"GP_MINIO_ENDPOINT"},
				Destination: &config.Minio.EndPoint,
			},
			&cli.BoolFlag{
				Name:        "minio-ssl",
				Usage:       "minio-ssl",
				EnvVars:     []string{"GP_MINIO_SSL"},
				Destination: &config.Minio.SSL,
			},
			&cli.StringFlag{
				Name:        "minio-bucket",
				Value:       "graduate",
				Usage:       "minio-bucket",
				EnvVars:     []string{"GP_MINIO_BUCKET"},
				Destination: &config.Minio.Bucket,
			},
			&cli.StringFlag{
				Name:        "minio-region",
				Value:       "us-east-1",
				Usage:       "minio-region",
				EnvVars:     []string{"GP_MINIO_REGION"},
				Destination: &config.Minio.Region,
			},
			&cli.BoolFlag{
				Name:        "twilio",
				Value:       false,
				Usage:       "twilio",
				EnvVars:     []string{"GP_SMS"},
				Destination: &config.SMS.Open,
			},
			&cli.StringFlag{
				Name:        "vault-token",
				Value:       "",
				Usage:       "vault-token",
				EnvVars:     []string{"GP_VAULT_TOKEN"},
				Destination: &config.Vault.Token,
			},
			&cli.BoolFlag{
				Name:        "On-Server",
				Value:       false,
				Usage:       "On-Server",
				EnvVars:     []string{"GP_SERVER_SITE"},
				Destination: &config.Server.OnSite,
			},
			&cli.StringFlag{
				Name:        "redis-host",
				Value:       "",
				Usage:       "redis-host",
				EnvVars:     []string{"GP_REDIS_HOST"},
				Destination: &config.Redis.Host,
			},
			&cli.StringFlag{
				Name:        "redis-port",
				Value:       "",
				Usage:       "redis-port",
				EnvVars:     []string{"GP_REDIS_PORT"},
				Destination: &config.Redis.Port,
			},
			&cli.StringFlag{
				Name:        "redis-addr",
				Value:       "",
				Usage:       "redis-addr",
				EnvVars:     []string{"GP_REDIS_ADDR"},
				Destination: &config.Redis.Addr,
			},
		},

		Action: func(c *cli.Context) error {
			logrus.Infof("Starting on %s", config.Server.Host)

			router.GlobalInit()
			server := &http.Server{
				Addr:         config.Server.Addr,
				Handler:      router.Load(),
				ReadTimeout:  50 * time.Second,
				WriteTimeout: 50 * time.Second,
			}
			if err := startServer(server); err != nil {
				logrus.Fatal(err)
			}
			return nil
		},
	}
}
