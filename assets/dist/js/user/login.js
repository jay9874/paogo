$(document).on('click', '#sendLoginForm', function(e){
    e.preventDefault(e);
    var formData = new FormData();

    $.each($("#loginForm").serializeArray(), function(i, val) {
        formData.append(val.name, val.value);
    });

$.ajax({
  method: "POST",
  processData: false,
  contentType: false,
  url: $("#loginForm").attr('action'),
  data: formData
}).done(function (res, textStatus, xhr) {
  if(res.Status=='Success')
  {
    localStorage.setItem('token', xhr.getResponseHeader('Authorization'));
    window.location.replace(window.location.protocol+"//"+ document.location.host + "/store/allStore");
  }
}).fail(function(err){
  if(err.responseJSON.code == 700) {
    alert("由於您一直嘗試登入,系統將會鎖定,請於一個小時後再試");
  }else{
    alert('登入失敗!請稍後再試');
  }
})

})