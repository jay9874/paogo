	 
	$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
        //document.oncopy = function(){ return false; };
        var jwtToken = localStorage.getItem('token');
        if (!jwtToken) {
        alertify.alert().setContent("<strong>請先登入!!</strong>").setHeader('<em> 系統訊息 </em> ').set({
        'transition':'slide',
        'modal': true,
        'movable': false,
        'pinnable': false,
        'closable': false,
        'onok': function(closeEvent){ 
            window.location.replace(window.location.protocol+"//"+ document.location.host+"/user/login");
          }
      }).show();
      return false;
	}
	$.ajax({
	method: "POST",
	beforeSend: function(request) {
	  request.setRequestHeader("Authorization", jwtToken);
	},
	url: "/user/detail",
  })
	.done(function( res ) {
        console.log(res);
	  $("#userName").text(res.UserName);
	  $("#studentID").append("<p class='text-center mg-md'>"+res.StudentID+"</p>");
	  $("#lineID").append("<p class='text-center mg-md'>"+res.LineID+"</p>");
    $('#email').append("<p class='text-center mg-md'>"+res.Email+"</p>");
      $('#credits').text(res.Credits);
      $('#userWallet').val(res.Wallet);
      $("#profile_img").attr("src", res.Image);
      $("#development").text(res.Development);
	  if(res.SMS == "True"){
      $('#sendSMS').prop( "checked",true );
    }
    $('#sendSMS').bootstrapSwitch();
	}).fail(function(jqXHR, textStatus){
        alertify.alert().setContent("<strong>登入逾時,請重新登入!!</strong>").setHeader('<em> 系統訊息 </em> ').set({
            'transition':'slide',
            'modal': true,
            'movable': false,
            'pinnable': false,
            'closable': false,
            'onok': function(closeEvent){ 
                window.location.replace(window.location.protocol+"//"+ document.location.host+"/user/login");
              }
          }).show();
  })
  
      // preview and hide password
    $("#showPassword").click(function(){
      var foo = $("#userWallet").attr("type");

      if(foo == "password"){
        $("#userWallet").attr("type", "text");

        $("#showPassword").tooltip('hide').attr('data-original-title', '隱藏錢包地址');
        $("#copyWallet").css("display", "block");
      } else {
        $("#userWallet").attr("type", "password");

        $("#showPassword").tooltip('hide').attr('data-original-title', '顯示錢包地址');
        $("#copyWallet").css("display", "none");
      }

    });

    $("#copyWallet").click(function(){
      var copyText = document.getElementById("userWallet");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("Copy");

      $("#copyWallet").tooltip('hide')
          .attr('data-original-title', '成功複製!')
          .tooltip('show');
    });

    $("#copyWallet").mouseout(function(){
      $("#copyWallet").attr('data-original-title', '點擊複製');
    });

    $("#showPassword").mouseout(function(){
      $("#copyWallet").tooltip('hide');
    });

  });
  
 