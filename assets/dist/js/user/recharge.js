
var jwtToken = localStorage.getItem('token'); // 驗證使用者登入狀態,是否有JWT Token

    if (!jwtToken) {
        alertify.alert().setContent("<strong>請先登入!!</strong>").setHeader('<em> 系統訊息 </em> ').set({
        'transition':'slide',
        'modal': true,
        'movable': false,
        'pinnable': false,
        'closable': false,
        'onok': function(closeEvent){ 
            window.location.replace(window.location.protocol+"//"+ document.location.host+"/user/login");
          }
      }).show();
  }


  $(document).on('click', '#sendCash', function(e){
    e.preventDefault(e);
    if (document.querySelector('#cashForm').checkValidity()) { // 檢查表單
  $.ajax({ // 打儲值API
  method: "POST",
  beforeSend: function(request) {
    request.setRequestHeader("Authorization", jwtToken);
  },
  url: "/user/cash/order",
  data: JSON.stringify({
          email: $('#Email').val(),
          amt: parseInt($('#Amt').val())
        })
})
  .done(function( res ) {
    $("#CheckValue").val(res.CheckValue);
    $("#MerchantOrderNo").val(res.MerchantOrderNo);
    $("#TimeStamp").val(res.TimeStamp);
    $('#cashForm').submit();
  }).fail(function(jqXHR, textStatus){
    alertify.alert().setContent("<strong>儲值失敗,請稍後再試!!</strong>").setHeader('<em> 系統訊息 </em> ').set({
        'transition':'slide',
        'modal': true,
        'movable': false,
        'pinnable': false,
        'closable': false,
        'onok': function(closeEvent){ 
            location.reload();
          }
      }).show();
  })
}else{
    document.querySelector('#cashForm').reportValidity(); // 表單填寫錯誤,跳出提示
}
});
