$( document ).ready(function() {
    var jwtToken = localStorage.getItem('token');
    if (!jwtToken) {
    alertify.alert().setContent("<strong>請先登入!!</strong>").setHeader('<em> 系統訊息 </em> ').set({
    'transition':'slide',
    'modal': true,
    'movable': false,
    'pinnable': false,
    'closable': false,
    'onok': function(closeEvent){ 
        window.location.replace(window.location.protocol+"//"+ document.location.host+"/user/login");
      }
  }).show();
  return false;
}
 var minute =moment().get('minute');

$('#apptime').attr({
   "max" : moment().add(3, 'hours').get('hour')+":"+minute,
   "min" : moment().get('hour')+":"+minute  
});

});
$(document).on('click', '#sendStore', function(e){
e.preventDefault(e);
var formData = new FormData();

$.each($("#storeForm").serializeArray(), function(i, val) {
    formData.append(val.name, val.value);
});
formData.delete('apptime');
formData.delete('shopimage');
var ins = document.getElementById('shopimage').files.length;
for (var x = 0; x < ins; x++) {
formData.append("shopimage[]", document.getElementById('shopimage').files[x]);
}

formData.append("deadline", moment().format("YYYY-MM-DD")+" "+$('#apptime').val());

$.ajax({
method: "POST",
processData: false,
contentType: false,
beforeSend: function(request) {
  request.setRequestHeader("Authorization", localStorage.getItem('token'));
},
url: "/store/create",
data: formData,
})
.done(function( res ) {
    console.log(res);
    if(res.Message === 'ok') {
        alertify.alert().setContent("<strong>商店建立成功</strong>").setHeader('<em> 系統訊息 </em> ').set({
            'transition':'slide',
            'modal': true,
            'movable': false,
            'pinnable': false,
            'closable': false,
            'onok': function(closeEvent){ 
                window.location.replace(window.location.protocol+"//"+ document.location.host+"/store/allStore");
            }
        }).show();
    }
}).fail(function(err){
    var notify = "商店建立失敗";
    if(err.responseJSON.code == 214)
    {
        notify = "任務已被其他人接訂";
    }
    if(err.responseJSON.code == 215)
    {
        notify = "不可接取自己的任務";
    }
    if(err.responseJSON.code == 211)
    {
        notify = "跑腿費高於帳戶餘額";
    }
    alertify.alert().setContent("<strong>"+notify+"</strong>").setHeader('<em> 系統訊息 </em> ').set({
            'transition':'slide',
            'modal': true,
            'movable': false,
            'pinnable': false,
            'closable': false,
            'onok': function(closeEvent){ 
                window.location.replace(window.location.protocol+"//"+ document.location.host+"/store/allStore");
            }
        }).show();
})
});